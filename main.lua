-- Copyright (c) 2039 Dark Energy Processor Corporation
--
-- This software is provided 'as-is', without any express or implied
-- warranty.  In no event will the authors be held liable for any damages
-- arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose,
-- including commercial applications, and to alter it and redistribute it
-- freely, subject to the following restrictions:
--
-- 1. The origin of this software must not be misrepresented; you must not
--    claim that you wrote the original software. If you use this software
--    in a product, an acknowledgment in the product documentation would be
--    appreciated but is not required.
-- 2. Altered source versions must be plainly marked as such, and must not be
--    misrepresented as being the original software.
-- 3. This notice may not be removed or altered from any source distribution.

-- Example Live2LOVE using Miku model file
local love = require("love")
local Live2LOVE = require("lib/Live2LOVE")

local Download = require("lib/download/Download")

require("lib/draw/Background")
require("lib/Math")

local vires = require("lib/draw/vires")

local mikuModel, mikuMotion, bg
local motionStr = "List of motions (press key number to change):\n"

function love.load()


	vires.init(960, 640)
	-- Update virtual resolution but using love.graphics.getDimensions value
	-- because we can't be sure that 960x640 is supported in mobile or
	-- in lower resolutions.
	vires.update(love.graphics.getDimensions())

	bg = LiveBG(Download("assets/bg/scenario2_rip/bg00339.png"))

	-- Load model. loadModel expects model definition (JSON file)
	mikuModel = Live2LOVE.loadModel("models/008_event_54_story_01/model.json")
	mikuModel2 = Live2LOVE.loadModel("models/008_live_event_75_sr/model.json")
	mikuModel3 = Live2LOVE.loadModel("models/008_swim_swit/model.json")
	-- Get list of motions
	mikuMotion = mikuModel:getMotionList()
	-- Format motions. Faster & better approach is possible to handle the strings.
	-- Note that the keyboard input only supports 10 keys (1-9, 0)
	for i = 1, table.getn(mikuMotion) do
		if not(mikuMotion[i]) then break end
		print(mikuMotion[i])
	end
	-- love.graphics.setBackgroundColor(0,255,0)
	-- Draw information
	print(string.format(
		"Live2LOVE v%s using Live2D Cubism SDK v%s",
		Live2LOVE._VERSION,
		Live2LOVE.Live2DVersion
		))
end

function love.update(dt)
	-- Update model
	mikuModel:update(dt)
	mikuModel2:update(dt)
	mikuModel3:update(dt)
end

function love.draw()
	bg:drawImage()

	-- Estimate the rendering per device
	local x, y = vires.logicalToScreen(0, -10)
	--mikuModel:draw(x, y, 0, vires.getScaling() * 0.25)
	local x, y = vires.logicalToScreen(200, -10)
	mikuModel2:draw(x, y, 0, vires.getScaling() * 0.25)
	local x, y = vires.logicalToScreen(400, -10)
	--mikuModel3:draw(x, y, 0, vires.getScaling() * 0.25)

	-- x, y, rotation, scaleX, scaleY
	--mikuModel:draw(width + (-getPercentage(width, 75)), -getPercentage(height, 5), 0, vires.getScaling() * 0.25)
	--mikuModel2:draw(width + (-getPercentage(width, 20)), -getPercentage(height, 5), 0, vires.getScaling() * 0.25)
	--mikuModel3:draw(width + (getPercentage(width, 35)), -getPercentage(height, 5), 0, vires.getScaling() * 0.25)

	--FPS
	local fps = love.timer.getFPS()
	love.graphics.print(
		{
			{
				0,0,0,255
			},
			string.format(
			"FPS: %d\n\nLive2LOVE v%s using Live2D Cubism SDK v%s",
			fps,
			Live2LOVE._VERSION,
			Live2LOVE.Live2DVersion
			)
		}
	)

end

function love.keyreleased(key)
	-- Only accept key numbers (not numlock one)
	if key == "escape" then love.event.quit() end
	if key == "f1" then love.load() end
	local keynum = tonumber(key)
	if not(keynum) then return end
	if keynum < 0 and keynum > 9 then return end
	if keynum == 0 then keynum = 10 end
	
	-- Play just once
	if mikuMotion[keynum] then
		mikuModel:setMotion(mikuMotion[keynum], "normal")
	end
end
