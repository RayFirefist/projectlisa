local mode = "iphone"

function love.conf(t)
    t.version = "11.0"
    t.window.title = "Live 2D - ProjectLisa"
    t.modules.audio = true
    t.modules.joystick = false
    t.modules.math = true
    t.modules.mouse = true
    t.modules.physics = false
    t.modules.sound = true
    t.modules.system = true
    t.modules.thread = true
    t.modules.touch = true
    t.modules.video = false

    t.window.fullscreen = love._os == "iOS" or love._os == "Android"

    if not(love._os == "iOS" or love._os == "Android") then
        if(mode == "ipad") then --iPad
            t.window.width = 1024
            t.window.height = 768
        elseif (mode == "iphonex") then --iPhoneX
            t.window.width = 1218
            t.window.height = 625
        else  --iPhone6
            t.window.width = 1280
            t.window.height = 720
        end
    end

    t.identity = "ProjectLisa"

    love.filesystem.setIdentity("ProjectLisa", true)
end