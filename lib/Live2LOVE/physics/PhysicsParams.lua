local path = (...):sub(1, #(...) - #(".physics.PhysicsParams"))
local Kasumy = require(path..".dummy")

local PhysicsSrc = Kasumy.Luaoop.class("Kasumy.PhysicsSrc")
local PhysicsTarget = Kasumy.Luaoop.class("Kasumy.PhysicsTarget")

-- PhysicsSrc
function PhysicsSrc:__construct(srcType, _paramID, _scale, _weight)
	self.srcType = srcType
	self.paramID = _paramID
	self.scale = _scale
	self.weight = _weight
end

function PhysicsSrc:updateSrc(model, hair)
	local scaleAffection = self.scale * model:getParamFloat(self.paramID)
	local p1 = hair:getPhysicsPoint1()

	if self.srcType == "SRC_TO_Y" then
		p1.y = p1.y + (scaleAffection - p1.y) * self.weight
	elseif self.srcType == "SRC_TO_G_ANGLE" then
		local grav = hair:getGravityAngleDeg()
		hair:setGravityAngleDeg(grav + (scaleAffection - grav) * self.weight)
	else
		p1.x = p1.x + (scaleAffection - p1.x) * self.weight
	end
end

-- PhysicsTarget
function PhysicsTarget:__construct(targetType, _paramID, _scale, _weight)
	self.targetType = targetType
	self.paramID = _paramID
	self.scale = _scale
	self.weight = _weight
end

function PhysicsTarget:updateTarget(model, hair)
	if self.targetType == "TARGET_FROM_ANGLE_V" then
		model:setParamFloat(self.paramID, self.scale * hair:getAngleP1toP2Deg(), self.weight)
	else
		model:setParamFloat(self.paramID, self.scale * hair:getAngleP1toP2Deg_velocity(), self.weight)
	end
end

return {
	PhysicsSrc,
	PhysicsTarget
}
