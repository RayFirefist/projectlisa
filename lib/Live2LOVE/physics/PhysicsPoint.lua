local path = (...):sub(1, #(...) - #(".physics.PhysicsPoint"))
local Kasumy = require(path..".dummy")

local PhysicsPoint
local PhysicsPoint_t

if Kasumy.ffi then
	PhysicsPoint = {}
	PhysicsPoint_t = Kasumy.ffi.typeof([[
		struct {
			double mass;
			double x, y;
			double vx, vy;
			double ax, ay;
			double fx, fy;
			double last_x, last_y;
			double last_vx, last_vy;
		}
	]])
else
	PhysicsPoint = Kasumy.Luaoop.class("Kasumy.PhysicsPoint")

	function PhysicsPoint:__construct()
		self.mass = 1
		self.x = 0
		self.y = 0
		self.vx = 0
		self.vy = 0
		self.ax = 0
		self.ay = 0
		self.fx = 0
		self.fy = 0
		self.last_x = 0
		self.last_y = 0
		self.last_vx = 0
		self.last_vy = 0
	end
end

function PhysicsPoint:setupLast()
	self.last_x = self.x
	self.last_y = self.y
	self.last_vx = self.vx
	self.last_vy = self.vy
end

if PhysicsPoint_t then
	Kasumy.ffi.metatype(PhysicsPoint_t, {__index = PhysicsPoint})
	setmetatable(PhysicsPoint, {
		__call = function()
			return PhysicsPoint_t({mass = 1})
		end
	})
end

return PhysicsPoint
