local path = (...):sub(1, #(...) - #(".physics.PhysicsHair"))
local Kasumy = require(path..".dummy")

local PhysicsSrc, PhysicsTarget
do
	local a = require(path..".physics.PhysicsParams")
	PhysicsSrc, PhysicsTarget = a[1], a[2]
end
local PhysicsPoint = require(path..".physics.PhysicsPoint")

local PhysicsHair = Kasumy.Luaoop.class("Kasumy.PhysicsHair")

local deg = 180 / math.pi
local atan2 = math.atan2 or math.atan
local sqrt = math.sqrt
local min = math.min
local cos = math.cos
local sin = math.sin
local rad = math.pi / 180

-- PhysicsHair
function PhysicsHair:__construct()
	self.p1 = PhysicsPoint()
	self.p2 = PhysicsPoint()
	self.baseLengthM = 0
	self.gravityAngleDeg = 0
	self.airResistance = 0
	self.angleP1toP2Deg = 0
	self.last_angleP1toP2Deg = 0
	self.angleP1toP2Deg_v = 0
	self.startTime = 0
	self.lastTime = 0
	self.srcListPtr = {}
	self.targetListPtr = {}
	self:setup(0.3, 0.5, 0.1)
end

function PhysicsHair:setup(baseLengthM, airRegistance, mass)
	self.last_angleP1toP2Deg = self:calc_angleP1toP2()
	self.p2:setupLast()

	if baseLengthM then
		self.baseLengthM = baseLengthM
		self.airResistance = airRegistance
		self.p1.mass = mass
		self.p2.mass = mass
		self.p2.y = baseLengthM
		self:setup()
	end
end

function PhysicsHair:getPhysicsPoint1()
	return self.p1
end

function PhysicsHair:getPhysicsPoint2()
	return self.p2
end

function PhysicsHair:getGravityAngleDeg()
	return self.gravityAngleDeg
end

function PhysicsHair:setGravityAngleDeg(n)
	self.gravityAngleDeg = n
end

function PhysicsHair:getAngleP1toP2Deg()
	return self.angleP1toP2Deg
end

function PhysicsHair:getAngleP1toP2Deg_velocity()
	return self.angleP1toP2Deg_v
end

function PhysicsHair:calc_angleP1toP2()
	-- I'm skeptical about the order (should be y/x)
	return -atan2(self.p1.x - self.p2.x, self.p2.y - self.p1.y) * deg
end

function PhysicsHair:addSrcParam(srcType, paramID, scale, weight)
	self.srcListPtr[#self.srcListPtr + 1] = PhysicsSrc(srcType, paramID, scale, weight)
end

function PhysicsHair:addTargetParam(targetType, paramID, scale, weight)
	self.targetListPtr[#self.targetListPtr + 1] = PhysicsTarget(targetType, paramID, scale, weight)
end

function PhysicsHair:update(model, time)
	if self.startTime == 0 then
		local p1, p2 = self.p1, self.p2
		local xd, yd = p1.x - p2.x, p1.y - p2.y
		self.startTime = time
		self.lastTime = time
		self.baseLengthM = sqrt(xd * xd + yd * yd)
		return
	end

	local dt = (time - self.lastTime) / 1000
	if dt > 0 then
		for i = 1, #self.srcListPtr do
			self.srcListPtr[i]:updateSrc(model, self)
		end

		-- Original Live2D approach when updating
		-- the physics is sucks (limiting dT to 30FPS)
		local t = dt
		while t > 0 do
			local upt = min(t, 1/30)
			self:update_exe(upt)
			t = t - upt
		end

		self.angleP1toP2Deg = self:calc_angleP1toP2()
		self.angleP1toP2Deg_v = (self.angleP1toP2Deg - self.last_angleP1toP2Deg) / dt
		self.last_angleP1toP2Deg = self.angleP1toP2Deg
	end

	for i = 1, #self.targetListPtr do
		self.targetListPtr[i]:updateTarget(model, self)
	end

	self.lastTime = time
end

function PhysicsHair:update_exe(dt)
	local fps = 1 / dt
	local p1, p2 = self.p1, self.p2

	p1.vx = (p1.x - p1.last_x) * fps
	p1.vy = (p1.y - p1.last_y) * fps
	p1.ax = (p1.vx - p1.last_vx) * fps
	p1.ay = (p1.vy - p1.last_vy) * fps
	p1.fx = p1.ax * p1.mass
	p1.fy = p1.ay * p1.mass
	p1:setupLast()
	local rot = -atan2(p1.y - p2.y, p1.x - p2.x)
	local c = cos(rot)
	local s = sin(rot)
	-- Better gravity constant yeah
	-- TODO: Let user adjust the gravity constant
	local weight = 9.80665 * p2.mass * cos(rot - rad * self.gravityAngleDeg)
	p2.fx = s * (weight - p1.fx * s) - p2.vx * self.airResistance
	p2.fy = c * (weight - p1.fy * s) - p2.vy * self.airResistance
	p2.ax = p2.fx / p2.mass
	p2.ay = p2.fy / p2.mass
	p2.vx = p2.vx + p2.ax * dt
	p2.vy = p2.vy + p2.ay * dt
	p2.x = p2.x + p2.vx * dt
	p2.y = p2.y + p2.vy * dt
	local xd, yd = p1.x - p2.x, p1.y - p2.y
	local dist = sqrt(xd * xd + yd * yd)
	p2.x = p1.x + self.baseLengthM * (-xd) / dist
	p2.y = p1.y + self.baseLengthM * (-yd) / dist
	p2.vx = (p2.x - p2.last_x) * fps
	p2.vy = (p2.y - p2.last_y) * fps
	p2:setupLast()
end

return PhysicsHair
