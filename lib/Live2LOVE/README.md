Kasumy
======
This is API-compatible re-implementation of Live2D and [Live2LOVE](https://github.com/MikuAuahDark/Live2LOVE) but written 100% in Lua, which means it's possible to view any Live2D (v2) models in Linux and macOS platform.

Other purpose of this library is to see how LuaJIT performs when doing complex model transformation and (at later time) compare the performance with the Javascript version, or maybe even the C++ version.

API
-----
There are 2 kinds of API. One that is similar to Live2D and one that is similar to Live2LOVE. Users are encouraged to use the latter as the former can make your brain twisted.
