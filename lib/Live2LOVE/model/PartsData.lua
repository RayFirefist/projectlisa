local path = (...):sub(1, #(...) - #(".model.PartsData"))
local Kasumy = require(path..".dummy")

local PartsData = Kasumy.Luaoop.class("Kasumy.PartsData")
local PartsDataContext = Kasumy.Luaoop.class("Kasumy.PartsDataContext")

-- PartsDataContext
function PartsDataContext:__construct(src)
	self.partsOpacity = 0
	self.srcPtr = src
end

function PartsDataContext:getPartsOpacity()
	return self.partsOpacity
end

function PartsDataContext:setPartsOpacity(opacity)
	self.partsOpacity = opacity
end

-- PartsData
PartsData.INSTANCE_COUNT = 0

function PartsData:__construct()
	self.visible = true
	self.locked = false
	self.partsID = nil
	self.baseDataList = nil
	self.drawDataList = nil
	PartsData.INSTANCE_COUNT = PartsData.INSTANCE_COUNT + 1
end

function PartsData:initDirect()
	self.baseDataList, self.drawDataList = {}, {}
end

function PartsData:readV2(br)
	self.locked = br:readBit()
	self.visible = br:readBit()
	self.partsID = br:readObject()
	self.baseDataList = br:readObject()
	self.drawDataList = br:readObject()
end

function PartsData:init()
	local context = PartsDataContext(self)
	context:setPartsOpacity(self.visible and 1 or 0)
	return context
end

function PartsData:addBaseData(baseData)
	assert(self.baseDataList, "base data list not initialized")
	self.baseDataList[#self.baseDataList + 1] = baseData
end

function PartsData:addDrawData(drawData)
	assert(self.drawDataList, "draw data list not initialized")
	self.drawDataList[#self.drawDataList + 1] = drawData
end

function PartsData:setBaseData(list)
	self.baseDataList = list
end

function PartsData:setDrawData(list)
	self.drawDataList = list
end

function PartsData:isVisible()
	return self.visible
end

function PartsData:isLocked()
	return self.locked
end

function PartsData:setVisible(visible)
	self.visible = visible
end

function PartsData:setLocked(locked)
	self.locked = locked
end

function PartsData:getBaseData()
	return self.baseDataList
end

function PartsData:getDrawData()
	return self.drawDataList
end

function PartsData:getPartsDataID()
	return self.partsID
end

function PartsData:setPartsDataID(id)
	self.partsID = id
end

PartsData.getPartsID = PartsData.getPartsDataID
PartsData.setPartsID = PartsData.setPartsDataID

return PartsData
