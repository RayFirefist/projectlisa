local path = (...):sub(1, #(...) - #(".model.ModelImpl"))
local Kasumy = require(path..".dummy")

local ParamDefSet = require(path..".param.ParamDefSet")

local ModelImpl = Kasumy.Luaoop.class("Kasumy.ModelImpl")
ModelImpl.INSTANCE_COUNT = 0

function ModelImpl:__construct()
	self.paramDefSet = nil
	self.partsDataList = nil
	self.canvasWidth = 400
	self.canvasHeight = 400
	ModelImpl.INSTANCE_COUNT = ModelImpl.INSTANCE_COUNT + 1
end

function ModelImpl:initDirect()
	if self.paramDefSet == nil then
		self.paramDefSet = ParamDefSet()
	end
	if self.partsDataList == nil then
		self.partsDataList = {}
	end
end

function ModelImpl:getCanvasWidth()
	return self.canvasWidth
end

function ModelImpl:getCanvasHeight()
	return self.canvasHeight
end

function ModelImpl:readV2(br)
	self.paramDefSet = br:readObject()
	self.partsDataList = br:readObject()
	self.canvasWidth = br:readInt()
	self.canvasHeight = br:readInt()
end

function ModelImpl:addPartsData(data)
	self.partsDataList[#self.partsDataList + 1] = data
end

function ModelImpl:getPartsDataList()
	return self.partsDataList
end

function ModelImpl:getParamDefSet()
	return self.paramDefSet
end

return ModelImpl
