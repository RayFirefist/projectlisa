local path = (...):sub(1, #(...) - #(".io.BReader"))
local Kasumy = require(path..".dummy")

local FileFormat2 = require(path..".io.FileFormat2")

local BReader = Kasumy.Luaoop.class("Kasumy.BReader")

---
-- @tparam string data
function BReader:__construct(data)
	self.bitCount = 0
	self.bitBuff = 0
	self.formatVersion = 0
	self.objects = {length = 0} -- can leave hole, so separate length must be used
	self.buffer = data
	self.pos = 0
end

function BReader:getFormatVersion()
	return self.formatVersion
end

---
-- @tparam number f
function BReader:setFormatVersion(f)
	self.formatVersion = f
end

function BReader:_read(n)
	local maxsize = math.min(self.pos + n, #self.buffer) - self.pos
	local str = self.buffer:sub(self.pos + 1, self.pos + maxsize)
	self.pos = self.pos + maxsize
	return str
end

function BReader:_newObject(id)
	if id == FileFormat2.NULL_NO then
		return nil
	elseif id == 1 or id == 50 or id == 51 or id == 134 or id == 60 then
		-- ID-related object (DrawDataID, BaseDataID, ...)
		-- For optimization (and simplicity purpose), use
		-- Lua string directly
		return self:readString()
	elseif id >= 48 then
		-- Live2D object
		local obj = FileFormat2.newInstance(id)
		if obj then
			obj:readV2(self)
		end
		return obj
	elseif id == 10 then
		-- LDColor
		return self:readInt()
	elseif id == 11 then
		-- LDRect
		return {
			x = self:readDouble(),
			y = self:readDouble(),
			width = self:readDouble(),
			height = self:readDouble()
		}
	elseif id == 12 then
		-- LDRect
		return {
			x = self:readFloat(),
			y = self:readFloat(),
			width = self:readFloat(),
			self:readFloat()
		}
	elseif id == 13 then
		-- LDPoint
		return {self:readDouble(), self:readDouble()}
	elseif id == 14 then
		-- LDPoint
		return {self:readFloat(), self:readFloat()}
	elseif id == 15 then
		-- Array of objects
		local count = self:readNum()
		local obj = {length = count}
		for i = 1, count do
			obj[i] = self:readObject()
		end
		return obj
	elseif id == 16 or id == 25 then
		return self:readArrayInt()
	elseif id == 17 then
		-- LDAffineTransform
		return {
			a = self:readDouble(),
			b = self:readDouble(),
			c = self:readDouble(),
			d = self:readDouble(),
			x = self:readDouble(),
			y = self:readDouble()
		}
	elseif id == 21 then
		-- LDRect
		return {
			x = self:readInt(),
			y = self:readInt(),
			width = self:readInt(),
			height = self:readInt()
		}
	elseif id == 22 then
		-- LDPoint
		return {self:readInt(), self:readInt()}
	elseif id == 23 then
		error("id 23 is reserved?")
	elseif id == 26 then
		return self:readArrayDouble()
	elseif id == 27 then
		return self:readArrayFloat()
	else
		error("cannot read id "..id)
	end
end

function BReader:readObject(cno)
	cno = cno or -1
	self:checkBits()

	if cno < 0 then
		cno = self:readNum()
	end

	if cno == FileFormat2.OBJECT_REF then
		local id = self:readInt()
		if id > 0 and id <= self.objects.length then
			return self.objects[id + 1] -- Lua is 1-based
		else
			error("object reference "..id.." doesn't exist (yet)", 2)
		end
	else
		local obj = self:_newObject(cno)
		self.objects.length = self.objects.length + 1
		self.objects[self.objects.length] = obj
		return obj
	end
end

function BReader:readBit()
	if self.bitCount == 0 or self.bitCount == 8 then
		self.bitBuff = self:readByte()
		self.bitCount = 0
	end

	local ret = math.floor(self.bitBuff / (2 ^ (7 - self.bitCount))) % 1 == 1
	self.bitCount = self.bitCount + 1
	return ret
end

function BReader:checkBits()
	self.bitCount = 0
end

function BReader:readNum()
	self:checkBits()
	local v = 0
	repeat
		local temp = self:readByte()
		v = (v * 128) + ((temp % 256) % 128)
	until temp >= 0
	return v
end

-- The read functions is mostly stolen from lua-MessagePack
function BReader:readFloat()
	self:checkBits()
	local b1, b2, b3, b4 = self:_read(4):byte(1, 4)
	local sign = (-1) ^ math.floor(b1 / 128)
	local expo = (b1 % 128) * 2 + math.floor(b2 / 128)
	local mant = ((b2 % 128) * 256 + b3) * 256 + b4

	if mant == 0 and expo == 0 then
		return sign * 0
	elseif expo == 255 then
		if mant == 0 then
			return sign * math.huge
		else
			return 0.0/0.0
		end
	else
		return sign * (1.0 + mant / 8388608) * 2 ^ (expo - 127)
	end
end

function BReader:readDouble()
	self:checkBits()
	local b1, b2, b3, b4, b5, b6, b7, b8 = self:_read(8):byte(1, 8)
	local sign = (-1) ^ math.floor(b1 / 128)
	local expo = (b1 % 128) * 16 + math.floor(b2 / 16)
	local mant = ((((((b2 % 16) * 256 + b3) * 256 + b4) * 256 + b5) * 256 + b6) * 256 + b7) * 256 + b8

	if mant == 0 and expo == 0 then
		return sign * 0
	elseif expo == 2047 then
		if mant == 0 then
			return sign * math.huge
		else
			return 0.0/0.0
		end
	else
		return sign * (1.0 + mant / 4503599627370496) * 2 ^ (expo - 1023)
	end
end

function BReader:readLong()
	self:checkBits()
	local a, b = 256, 255 -- hopefully constant folding does it's job
	local b1, b2, b3, b4, b5, b6, b7, b8 = self:_read(8):byte(1, 8)
	if b1 < 128 then
		return ((((((b1 * a + b2) * a + b3) * a + b4) * a + b5) * a + b6) * a + b7) * a + b8
	else
		return ((((((((b1-b)*a+(b2-b))*a+(b3-b))*a+(b4-b))*a+(b5-b))*a+(b6-b))*a+(b7-b))*a+(b8-b))-1
	end
end

function BReader:readInt()
	self:checkBits()
	local b1, b2, b3, b4 = self:_read(4):byte(1, 4)
	if b1 < 128 then
		return ((b1 * 256 + b2) * 256 + b3) * 256 + b4
	else
		return ((((b1 - 255) * 256 + (b2 - 255)) * 256 + (b3 - 255)) * 256 + (b4 - 255)) - 1
	end
end

function BReader:readBoolean()
	self:checkBits()
	return self:_read(1) ~= "\0"
end

function BReader:readByte()
	self:checkBits()
	local v = self:_read(1):byte()
	return v < 128 and v or (v - 256)
end

function BReader:readShort()
	self:checkBits()
	local b1, b2 = self:_read(2):byte(1, 2)
	return b1 < 128 and (b1 * 256 + b2) or (((b1 - 255) * 256 + (b2 - 255)) - 1)
end

function BReader:readArrayDouble()
	self:checkBits()
	local count = self:readNum()
	local ret = {}
	for i = 1, count do
		ret[i] = self:readDouble()
	end
	return ret
end

function BReader:readArrayFloat()
	self:checkBits()
	local count = self:readNum()
	local ret = {}
	for i = 1, count do
		ret[i] = self:readFloat()
	end
	return ret
end

function BReader:readArrayInt()
	self:checkBits()
	local count = self:readNum()
	local ret = {}
	for i = 1, count do
		ret[i] = self:readInt()
	end
	return ret
end

function BReader:readString()
	self:checkBits()
	return self:_read(self:readNum())
end

return BReader
