-- This is where the real fun begins
--local love = require("love")
local path = (...):sub(1, #(...) - #(".Live2DModelLOVE"))
local Kasumy = require(path..".dummy")

local ALive2DModel = require(path..".ALive2DModel")
local UtSystem = require(path..".util.UtSystem")

local Live2DModelLOVE = Kasumy.Luaoop.class("Kasumy.Live2DModelLOVE", ALive2DModel)

function Live2DModelLOVE:__construct()
	ALive2DModel.__construct(self)
	self.textures = {}
end

function Live2DModelLOVE.loadModel(data)
	data = UtSystem.getFileContents(data)

	local instance = Live2DModelLOVE()
	ALive2DModel.loadModel_exe(instance, data)
	return instance
end

function Live2DModelLOVE:setTexture(no, image)
	self.textures[no] = image
end

return Live2DModelLOVE
