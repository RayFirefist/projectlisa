local path = (...):sub(1, #(...) - #(".ALive2DModel"))
local Kasumy = require(path..".dummy")

local FileFormat2 = require(path..".io.FileFormat2")
local BReader = require(path..".io.BReader")
local ModelImpl = require(path..".model.ModelImpl")
local ModelContext = require(path..".ModelContext")

local ALive2DModel = Kasumy.Luaoop.class("Kasumy.Live2D.ALive2DModel")

ALive2DModel.FILE_LOAD_EOF_ERROR = 1
ALive2DModel.FILE_LOAD_VERSION_ERROR = 2
ALive2DModel.INSTANCE_COUNT = 0

function ALive2DModel:__construct()
	self.modelImpl = nil
	self.errorFlags = 0
	ALive2DModel.INSTANCE_COUNT = ALive2DModel.INSTANCE_COUNT + 1
	self.modelContext = ModelContext(self)
end

function ALive2DModel:loadModel_exe(buf)
	local br = BReader(buf)
	local h1 = br:readByte()
	local h2 = br:readByte()
	local h3 = br:readByte()
	local version

	if h1 == 109 and h2 == 111 and h3 == 99 then
		version = br:readByte()
	else
		error("invalid model file")
	end

	br:setFormatVersion(version)
	if version > FileFormat2.LIVE2D_FORMAT_VERSION_AVAILABLE then
		self.errorFlags = self.errorFlags + ALive2DModel.FILE_LOAD_VERSION_ERROR
		error("version error (model "..version..", current "..FileFormat2.LIVE2D_FORMAT_VERSION_AVAILABLE..")")
	end

	local modelImpl = br:readObject()
	if version >= FileFormat2.LIVE2D_FORMAT_VERSION_V2_8_TEX_OPTION then
		if br:readInt()%4294967296 ~= FileFormat2.LIVE2D_FORMAT_EOF_VALUE then
			self.errorFlags = self.errorFlags + ALive2DModel.FILE_LOAD_EOF_ERROR
			error("model loading error, eof mark not detected")
		end
	end

	self:setModelImpl(modelImpl)
	local mdc = self:getModelContext()
	--mdc:setDrawParam(self:getDrawParam())
	return mdc:init()
end

function ALive2DModel:setModelImpl(modelImpl)
	self.modelImpl = modelImpl
end

function ALive2DModel:getModelImpl()
	if not(self.modelImpl) then
		self.modelImpl = ModelImpl()
		self.modelImpl:initDirect()
	end

	return self.modelImpl
end

function ALive2DModel:getCanvasWidth()
	return self.modelImpl and self.modelImpl:getCanvasWidth() or 0
end

function ALive2DModel:getCanvasHeight()
	return self.modelImpl and self.modelImpl:getCanvasHeight() or 0
end

function ALive2DModel:getParamFloat(id)
	if type(id) ~= "number" then
		id = self.modelContext:getParamIndex(id)
	end

	return self.modelContext:getParamFloat(id)
end

function ALive2DModel:setParamFloat(id, value, weight)
	if type(id) ~= "number" then
		id = self.modelContext:getParamIndex(id)
	end
	weight = weight or 1
	return self.modelContext:setParamFloat(id, self.modelContext:getParamFloat(id) * (1 - weight) + value * weight)
end

function ALive2DModel:addToParamFloat(id, value, weight)
	if type(id) ~= "number" then
		id = self.modelContext:getParamIndex(id)
	end
	weight = weight or 1
	return self.modelContext:setParamFloat(id, self.modelContext:getParamFloat(id) + value * weight)
end

function ALive2DModel:multParamFloat(id, value, weight)
	if type(id) ~= "number" then
		id = self.modelContext:getParamIndex(id)
	end
	weight = weight or 1
	return self.modelContext:setParamFloat(id, self.modelContext:getParamFloat(id) * (1 + (value - 1) * weight))
end

function ALive2DModel:getParamIndex(id)
	return self.modelContext:getParamIndex(id)
end

function ALive2DModel:loadParam()
	return self.modelContext:loadParam()
end

function ALive2DModel:saveParam()
	return self.modelContext:saveParam()
end

function ALive2DModel:init()
	return self.modelContext:init()
end

function ALive2DModel:update()
	return self.modelContext:update()
end

function ALive2DModel.draw()
end

function ALive2DModel:getModelContext()
	return self.modelContext
end

function ALive2DModel:getErrorFlags()
	return self.errorFlags
end

function ALive2DModel:setPartsOpacity(id, opacity)
	if type(id) ~= "number" then
		id = self.modelContext:getPartsDataIndex(id)
	end
	return self.modelContext:setPartsOpacity(id, opacity)
end

function ALive2DModel:getPartsOpacity(id)
	if type(id) ~= "number" then
		id = self.modelContext:getPartsDataIndex(id)
	end
	return self.modelContext:getPartsOpacity(id)
end

function ALive2DModel:getPartsDataIndex(id)
	return self.modelContext:getPartsDataIndex(id)
end

function ALive2DModel:getDrawDataIndex(id)
	return self.modelContext:getDrawDataIndex(id)
end

function ALive2DModel:getDrawData(index)
	return self.modelContext:getDrawData(index)
end

function ALive2DModel:getTransformedPoints(index)
	local context = self.modelContext:getDrawContext(index)
	-- assume context is DDTextureContext
	return context:getTransformedPoints()
end

function ALive2DModel:getIndexArray(index)
	local drawData = self.modelContext.drawDataList[index]
	if drawData then
		-- assume drawData is DDTexture
		return drawData:getIndexArray()
	end

	return nil
end

return ALive2DModel
