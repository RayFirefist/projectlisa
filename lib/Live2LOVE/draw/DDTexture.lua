local path = (...):sub(1, #(...) - #(".draw.DDTexture"))
local Kasumy = require(path..".dummy")

local IDrawData = require(path..".draw.IDrawData")
local IDrawContext = require(path..".draw.IDrawContext")
local FileFormat2 = require(path..".io.FileFormat2")
local PivotManager = require(path..".param.PivotManager")
local UtInterpolate = require(path..".util.UtInterpolate")

local DDTextureContext = Kasumy.Luaoop.class("Kasumy.DDTextureContext", IDrawContext)
local DDTexture = Kasumy.Luaoop.class("Kasumy.DDTexture", IDrawData)

local newDouble

if Kasumy.ffi then
	function newDouble(n)
		return Kasumy.ffi.new("double[?]", n + 1)
	end
else
	function newDouble(n)
		local temp = {}
		for i = 1, n do temp[i] = 0 end
		return temp
	end
end

-- DDTextureContext
function DDTextureContext:__construct(src)
	IDrawContext.__construct(self, src)
	self.tmpBaseDataIndex = IDrawData.BASE_INDEX_NOT_INIT;
	self.interpolatedPoints = nil
	self.transformedPoints = nil
	self.numPts = 0
end

function DDTextureContext:getTransformedPoints()
	return (self.transformedPoints or self.interpolatedPoints), self.numPts
end

-- DDTexture
IDrawContext.TYPE_DD_TEXTURE = DDTexture

DDTexture.INSTANCE_COUNT = 0
DDTexture.MASK_COLOR_COMPOSITION = 30
DDTexture.COLOR_COMPOSITION_NORMAL = 0
DDTexture.COLOR_COMPOSITION_SCREEN = 1
DDTexture.COLOR_COMPOSITION_MULTIPLY = 2

function DDTexture:__construct()
	IDrawData.__construct(self)
	DDTexture.INSTANCE_COUNT = DDTexture.INSTANCE_COUNT + 1
	self.textureNo = -1
	self.numPts = 0
	self.numPolygons = 0
	self.optionFlag = 0
	self.option = {}
	self.indexArray = nil
	self.pivotPoints = nil
	self.uvmap = nil
	self.colorCompositionType = DDTexture.COLOR_COMPOSITION_NORMAL
	self.culling = true
	self.gl_cacheImage = nil
	self.instanceNo = DDTexture.INSTANCE_COUNT
end

function DDTexture:setTextureNo(no)
	self.textureNo = no
end

function DDTexture:getTextureNo()
	return self.textureNo
end

function DDTexture:getUvMap()
	return self.uvmap
end

function DDTexture:getOptionFlag()
	return self.optionFlag
end

function DDTexture:getNumPoints()
	return self.numPts
end

function DDTexture.getType()
	return DDTexture
end

function DDTexture.setZ_TestImpl()
	-- TODO: Enable this?
	error("setZ_TestImpl is not enabled")
end

function DDTexture:initDirect()
	self.pivotManager = PivotManager()
	self.pivotManager:initDirect()
end

function DDTexture:readV2(br)
	IDrawData.readV2(self, br)
	self.textureNo = br:readInt()
	self.numPts = br:readInt()
	self.numPolygons = br:readInt()
	self.indexArray = br:readObject()
	self.pivotPoints = br:readObject()
	self.uvmap = br:readObject()

	if Kasumy.ffi then
		-- pivot points are nested doubles, have to be careful with this
		local ph, pw = #self.pivotPoints + 1, #self.pivotPoints[1] + 1

		-- recreate uvmap
		local uvmap = Kasumy.ffi.new("double[?]", ph * pw)
		for i = 1, #self.uvmap do
			uvmap[i] = self.uvmap[i]
		end
		self.uvmap = uvmap

		-- use uvmap pointer for storage
		local pp = Kasumy.ffi.new("double*[?]", ph)
		for i = 1, #self.pivotPoints do
			for j = 1, #self.pivotPoints[i] do
				uvmap[i * pw + j] = self.pivotPoints[i][j]
			end
			pp[i] = uvmap + i * pw
		end
		self.pivotPoints = pp

		-- recreate index array
		self.indexArray[0] = #self.indexArray
		self.indexArray = Kasumy.ffi.new("uint16_t[?]", #self.indexArray + 1, self.indexArray)
	end

	if br:getFormatVersion() >= FileFormat2.LIVE2D_FORMAT_VERSION_V2_8_TEX_OPTION then
		self.optionFlag = br:readInt()

		if self.optionFlag % 1 == 1 then
			self.option.KanojoColor = br:readInt()
		end

		-- TODO: optimize this, while preventing bit/bit32 library dependency
		if math.floor(self.optionFlag / 2) % (DDTexture.MASK_COLOR_COMPOSITION / 2 + 1) ~= 0 then
			self.colorCompositionType = math.floor(self.optionFlag) % (DDTexture.MASK_COLOR_COMPOSITION / 2 + 1)
		end

		if self.optionFlag / 32 >= 1 then
			self.culling = false
		end
	end
end

function DDTexture:init()
	local drawDataContext = DDTextureContext(self)
	drawDataContext.numPts = self.numPts

	if drawDataContext.interpolatedPoints then
		drawDataContext.interpolatedPoints = nil
	end
	if drawDataContext.transformedPoints then
		drawDataContext.transformedPoints = nil
	end

	drawDataContext.interpolatedPoints = newDouble(self.numPts * 2)
	if self:needTransform() then
		drawDataContext.transformedPoints = newDouble(self.numPts * 2)
	end

	return drawDataContext
end

function DDTexture:setupInterpolate(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, DDTextureContext), "cdata is not DDTextureContext")
	assert(cdata:getSrcPtr() == self, "invalid IDrawContext")

	if self.pivotManager:checkParamUpdated(mdc) then
		IDrawData.setupInterpolate(self, mdc, cdata)
		if cdata.paramOutside then return end
		UtInterpolate.interpolatePoints(mdc, self.pivotManager, self.numPts, self.pivotPoints, cdata.interpolatedPoints, 0, 2)
	end
end

function DDTexture:setupTransform(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, DDTextureContext), "cdata is not DDTextureContext")
	assert(cdata:getSrcPtr() == self, "invalid IDrawContext")

	if not(cdata.paramOutside) then
		IDrawData.setupTransform(self, mdc)

		if self:needTransform() then
			local target = self:getTargetBaseDataID()
			if cdata.tmpBaseDataIndex == IDrawData.BASE_INDEX_NOT_INIT then
				cdata.tmpBaseDataIndex = mdc:getBaseDataIndex(target)
			end

			if cdata.tmpBaseDataIndex < 0 then
				error("tmp base data index is negative: "..target)
			end

			local aO = mdc:getBaseData(cdata.tmpBaseDataIndex);
			local aJ = mdc:getBaseContext(cdata.tmpBaseDataIndex);
			if aO and not(aJ:isOutsideParam()) then
				aO:transformPoints(mdc, aJ, cdata.interpolatedPoints, cdata.transformedPoints, self.numPts)
				cdata.available = true;
			else
				cdata.available = false;
			end
			cdata.baseOpacity = aJ:getTotalOpacity()
		end
	end
end

function DDTexture:draw(dp, mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, DDTextureContext), "cdata is not DDTextureContext")
	assert(cdata:getSrcPtr() == self, "invalid IDrawContext")
	if cdata.paramOutside then return end

	local texture = math.max(self.textureNo, 1)
	local opacity = self:getOpacity(mdc, cdata) * cdata.partsOpacity * cdata.baseOpacity
	local points = cdata.transformedPoints or cdata.interpolatePoints
	dp:setClipBufPre_clipContextForDraw(cdata.clipBufPre_clipContext)
	dp:setCulling(self.culling)
	dp:drawTexture(
		texture,
		3 * self.numPolygons,
		self.indexArray,
		points,
		self.uvmap,
		opacity,
		self.colorCompositionType,
		cdata
	)
end

function DDTexture:getExtraOption(name)
	return not(not(self.option[name]))
end

function DDTexture:getIndexArray()
	return self.indexArray, 3 * self.numPolygons
end

return DDTexture
