local path = (...):sub(1, #(...) - #(".draw.IDrawContext"))
local Kasumy = require(path..".dummy")

local IDrawContext = Kasumy.Luaoop.class("Kasumy.IDrawContext")

function IDrawContext:__construct(src)
	self.srcPtr = nil
	self.partsIndex = nil
	self.interpolatedDrawOrder = nil
	self.interpolatedOpacity = nil
	self.paramOutside = false
	self.partsOpacity = nil
	self.available = true
	self.baseOpacity = 1
	self.clipBufPre_clipContext = nil
	self.srcPtr = src
end

function IDrawContext:isParamOutside()
	return self.paramOutside
end

function IDrawContext:isAvailable()
	return self.available and not(self.paramOutside)
end

function IDrawContext:getSrcPtr()
	return self.srcPtr
end

function IDrawContext:setPartsIndex(p)
	self.partsIndex = p
end

function IDrawContext:getPartsIndex()
	return self.partsIndex
end

return IDrawContext
