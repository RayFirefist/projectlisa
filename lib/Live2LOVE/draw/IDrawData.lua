local path = (...):sub(1, #(...) - #(".draw.IDrawData"))
local Kasumy = require(path..".dummy")

local FileFormat2 = require(path..".io.FileFormat2")
local UtInterpolate = require(path..".util.UtInterpolate")

local IDrawData = Kasumy.Luaoop.class("Kasumy.IDrawData")

IDrawData.BASE_INDEX_NOT_INIT = -2
IDrawData.DEFAULT_ORDER = 500
IDrawData.TYPE_DD_TEXTURE = nil -- set by DDTexture.lua

IDrawData.totalMinOrder = IDrawData.DEFAULT_ORDER
IDrawData.totalMaxOrder = IDrawData.DEFAULT_ORDER

function IDrawData.getTotalMinOrder()
	return IDrawData.totalMinOrder
end

function IDrawData.getTotalMaxOrder()
	return IDrawData.totalMaxOrder
end

function IDrawData:__construct()
	self.drawDataID = nil
	self.targetBaseDataID = nil
	self.pivotManager = nil
	self.averageDrawOrder = nil
	self.pivotDrawOrder = nil
	self.pivotOpacity = nil
	self.clipID = nil
	self.clipIDList = {}
end

function IDrawData.convertClipIDForV2_11(str)
	if not(str) or #str == 0 then return nil end

	local list = {}
	for w in str:gmatch("[^,]+") do
		list[#list + 1] = w
	end

	return list
end

function IDrawData:readV2(br)
	self.drawDataID = br:readObject();
	self.targetBaseDataID = br:readObject();
	self.pivotManager = br:readObject();
	self.averageDrawOrder = br:readInt();
	self.pivotDrawOrder = br:readArrayInt();
	self.pivotOpacity = br:readArrayFloat();

	if Kasumy.ffi then
		self.pivotDrawOrder[0] = #self.pivotDrawOrder
		self.pivotDrawOrder = Kasumy.ffi.new("int[?]", #self.pivotDrawOrder + 1, self.pivotDrawOrder)

		self.pivotOpacity[0] = #self.pivotOpacity
		self.pivotOpacity = Kasumy.ffi.new("double[?]", #self.pivotOpacity + 1, self.pivotOpacity)
	end

	if br:getFormatVersion() >= FileFormat2.LIVE2D_FORMAT_VERSION_AVAILABLE then
		self.clipID = br:readObject();
		self.clipIDList = IDrawData.convertClipIDForV2_11(self.clipID);
	end

	self:setDrawOrder(self.pivotDrawOrder);
end

function IDrawData:getClipIDList()
	return self.clipIDList
end

function IDrawData.init()
	error("pure virtual method 'IDrawData:init'")
end

function IDrawData:setupInterpolate(mdc, cdata)
	cdata.interpolatedDrawOrder, cdata.paramOutside = UtInterpolate.interpolateInt(
		mdc, self.pivotManager, self.pivotDrawOrder
	)

	if not(cdata.paramOutside) then
		cdata.interpolatedOpacity = UtInterpolate.interpolateFloat(mdc, self.pivotManager, self.pivotOpacity)
	end
end

function IDrawData.setupTransform()
end

function IDrawData:getDrawDataID()
	return self.drawDataID
end

function IDrawData:setDrawDataID(str)
	self.drawDataID = str
end

function IDrawData.getOpacity(_, _, cdata)
	return cdata.interpolatedOpacity
end

function IDrawData.getDrawOrder(_, _, cdata)
	return cdata.interpolatedDrawOrder
end

function IDrawData.setDrawOrder(_, orders)
	for i = 1, (orders[0] or #orders) do
		local order = orders[i]
		IDrawData.totalMinOrder = math.min(order, IDrawData.totalMinOrder)
		IDrawData.totalMaxOrder = math.max(order, IDrawData.totalMaxOrder)
	end
end

function IDrawData:getTargetBaseDataID()
	return self.targetBaseDataID
end

function IDrawData:setTargetBaseDataID(id)
	self.targetBaseDataID = id
end

function IDrawData:needTransform()
	return self.targetBaseDataID and self.targetBaseDataID ~= "DST_BASE"
end

function IDrawData.preDraw()
	error("pure virtual method 'IDrawData:preDraw'")
end

function IDrawData.draw()
	error("pure virtual method 'IDrawData:draw'")
end

function IDrawData.setZ_TestImpl()
end

return IDrawData
