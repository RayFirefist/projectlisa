local love = require("love")
local path = (...):sub(1, #(...) - #(".graphics.ClippingManagerOpenGL"))
local Kasumy = require(path..".dummy")

local ClippingManagerOpenGL = Kasumy.Luaoop.class("Kasumy.ClippingManagerOpenGL")

return ClippingManagerOpenGL
