local path = (...):sub(1, #(...) - #(".framework.Physics"))
local Kasumy = require(path..".dummy")

local PhysicsHair = require(path..".physics.PhysicsHair")
local UtSystem = require(path..".util.UtSystem")

local Physics = Kasumy.Luaoop.class("Kasumy.Framework.Physics", PhysicsHair)

function Physics:__construct()
	PhysicsHair.__construct(self)
	self.startTimeMSec = UtSystem.getUserTimeMSec()
	self.physicsObjects = {}
	self.paramIDs = {}
end

function Physics.load(f)
	f = UtSystem.getFileContents(f, 'load', 1)
	local json = Kasumy.JSON:decode(f)
	local ret = Physics()

	for _, param in ipairs(assert(json.physics_hair, "missing 'physics_hair' field")) do
		local setup = assert(param.setup, "missing 'setup' field")
		local hair = PhysicsHair()
		hair:setup(
			assert(setup.length, "missing 'length' in 'setup' field"),
			assert(setup.regist, "missing 'regist' in 'setup' field"),
			assert(setup.mass, "missing 'mass' field in 'setup'")
		)

		-- src
		for _, src in ipairs(assert(param.src, "missing 'src' field")) do
			local srcType = "SRC_TO_X"
			local paramID = assert(src.id, "missing 'id' in 'src' field")
			local typeStr = assert(src.ptype, "missing 'ptype' in 'src' field")
			local scale = assert(tonumber(src.scale), "missing 'scale' in 'src' field")
			local weight = assert(tonumber(src.weight), "missing 'weight' in 'src' field")

			if typeStr == "x" then srcType = "SRC_TO_X"
			elseif typeStr == "y" then srcType = "SRC_TO_Y"
			elseif typeStr == "angle" then srcType = "SRC_TO_G_ANGLE"
			else error("invalid src 'ptype' ("..typeStr..")") end

			ret.paramIDs[#ret.paramIDs + 1] = paramID
			hair:addSrcParam(srcType, paramID, scale, weight)
		end

		-- target
		for _, target in ipairs(assert(param.targets, "missing 'targets' field")) do
			local targetType = "TARGET_FROM_ANGLE"
			local paramID = assert(target.id, "missing 'id' in 'src' field")
			local typeStr = assert(target.ptype, "missing 'ptype' in 'src' field")
			local scale = assert(tonumber(target.scale), "missing 'scale' in 'src' field")
			local weight = assert(tonumber(target.weight), "missing 'weight' in 'src' field")

			if typeStr == "angle" then targetType = "TARGET_FROM_ANGLE"
			elseif typeStr == "angle_v" then targetType = "TARGET_FROM_ANGLE_V"
			else error("invalid target 'ptype' ("..typeStr..")") end

			ret.paramIDs[#ret.paramIDs + 1] = paramID
			hair:addTargetParam(targetType, paramID, scale, weight)
		end

		ret.physicsObjects[#ret.physicsObjects + 1] = hair
	end

	return ret
end

function Physics:updateParam(model)
	local timeMSec = UtSystem.getUserTimeMSec() - self.startTimeMSec
	for i = 1, #self.physicsObjects do
		self.physicsObjects[i]:update(model, timeMSec)
	end
end

return Physics
