local path = (...):sub(1, #(...) - #(".framework.ExpressionMotion"))
local Kasumy = require(path..".dummy")

local AMotion = require(path..".motion.AMotion")
local UtSystem = require(path..".util.UtSystem")

local ExpressionMotion = Kasumy.Luaoop.class("Kasumy.Framework.ExpressionMotion", AMotion)

ExpressionMotion.EXPRESSION_DEFAULT = "DEFAULT"
ExpressionMotion.TYPE_SET = 0
ExpressionMotion.TYPE_ADD = 1
ExpressionMotion.TYPE_MULT = 2

function ExpressionMotion:__construct()
	AMotion.__construct(self)
	self.paramList = {}
end

function ExpressionMotion:updateParamExe(model, _, weight, _)
	for i = 1, #self.paramList do
		local param = self.paramList[i]

		if param.type == ExpressionMotion.TYPE_ADD then
			model:addToParamFloat(param.pid, param.value, weight)
		elseif param.type == ExpressionMotion.TYPE_MULT then
			model:multParamFloat(param.pid, param.value, weight)
		elseif param.type == ExpressionMotion.TYPE_SET then
			model:setParamFloat(param.pid, param.value, weight)
		end
	end
end

function ExpressionMotion.loadJson(f)
	f = UtSystem.getFileContents(f, 'loadJson', 1)
	local json = Kasumy.JSON:decode(f)
	local ret = ExpressionMotion()

	ret:setFadeIn(tonumber(json.fade_in) or 1000)
	ret:setFadeOut(tonumber(json.fade_out) or 1000)

	if json.params then
		for _, v in ipairs(json.params) do
			local paramID = assert(v.id, "missing 'id' in 'params' field")
			local value = assert(tonumber(v.val), "missing 'val' in 'params' field")
			local calcType

			if v.calc == "mult" then
				local defaultValue = tonumber(v.def) or 1
				if defaultValue == 0 then defaultValue = 1 end
				value = value / defaultValue
				calcType = ExpressionMotion.TYPE_MULT
			elseif v.calc == "set" then
				calcType = ExpressionMotion.TYPE_SET
			else
				local defaultValue = tonumber(v.def) or 0
				value = value - defaultValue
				calcType = ExpressionMotion.TYPE_ADD
			end

			ret.paramList[#ret.paramList + 1] = {
				pid = paramID,
				type = calcType,
				value = value
			}
		end
	end

	return ret
end

return ExpressionMotion
