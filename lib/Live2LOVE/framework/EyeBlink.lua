local path = (...):sub(1, #(...) - #(".framework.EyeBlink"))
local Kasumy = require(path..".dummy")

local UtSystem = require(path..".util.UtSystem")

local STATE_FIRST = 0
local STATE_INTERVAL = 1
local STATE_CLOSING = 2
local STATE_CLOSED = 3
local STATE_OPENING = 4

local EyeBlink
local EyeBlink_t = nil

if Kasumy.ffi then
	EyeBlink_t = Kasumy.ffi.typeof([[struct {
		int32_t eyeState;
		int32_t blinkIntervalMsec;
		int32_t closingMotionMsec;
		int32_t closedMotionMsec;
		int32_t openingMotionMsec;
		int32_t nextBlinkTime;
		bool closeIfZero;
	}]])
	EyeBlink = {}
else
	EyeBlink = Kasumy.Luaoop.class("Kasumy.Framework.EyeBlink")
end

function EyeBlink:__construct()
	self.eyeState = STATE_FIRST
	self.blinkIntervalMsec = 4000
	self.closingMotionMsec = 100
	self.closedMotionMsec  =  50
	self.openingMotionMsec = 150
	self.nextBlinkTime = 0
	self.closeIfZero = true
	-- Hardcode eyeID atm
	--self.eyeID_L = "PARAM_EYE_L_OPEN"
	--self.eyeID_R = "PARAM_EYE_R_OPEN"
end

function EyeBlink:calcNextBlink()
	return UtSystem.getUserTimeMSec() + (math.random() * (2 * self.blinkIntervalMsec - 1))
end

function EyeBlink:setInterval(v)
	self.blinkIntervalMsec = v
end

function EyeBlink:setEyeMotion(closingMotionMsec, closedMotionMsec, openingMotionMsec)
	self.closingMotionMsec = closingMotionMsec
	self.closedMotionMsec = closedMotionMsec
	self.openingMotionMsec = openingMotionMsec
end

function EyeBlink:setParam(model)
	local time = UtSystem.getUserTimeMSec()
	local eyeParamValue

	if self.eyeState == STATE_CLOSING then
		local t = (time - self.stateStartTime) / self.closingMotionMsec
		if t >= 1 then
			t = 1
			self.eyeState = STATE_CLOSED
			self.stateStartTime = time
		end

		eyeParamValue = 1 - t
	elseif self.eyeState == STATE_CLOSED then
		local t = (time - self.stateStartTime) / self.closedMotionMsec
		if t >= 1 then
			self.eyeState = STATE_OPENING
			self.stateStartTime = time
		end

		eyeParamValue = 0
	elseif self.eyeState == STATE_OPENING then
		local t = (time - self.stateStartTime) / self.openingMotionMsec
		if t >= 1 then
			t = 1
			self.eyeState = STATE_INTERVAL
			self.nextBlinkTime = self:calcNextBlink()
		end

		eyeParamValue = t
	elseif self.eyeState == STATE_INTERVAL then
		if self.nextBlinkTime < time then
			self.eyeState = STATE_CLOSING
			self.stateStartTime = time
		end

		eyeParamValue = 1
	else
		self.eyeState = STATE_INTERVAL
		self.nextBlinkTime = self:calcNextBlink()
		eyeParamValue = 1
	end

	if not(self.closeIfZero) then eyeParamValue = -eyeParamValue end
	-- Hardcode eyeID atm
	--model:setParamFloat(self.eyeID_L, eyeParamValue)
	--model:setParamFloat(self.eyeID_R, eyeParamValue)
	model:setParamFloat("PARAM_EYE_L_OPEN", eyeParamValue)
	model:setParamFloat("PARAM_EYE_R_OPEN", eyeParamValue)
end

if EyeBlink_t then
	Kasumy.ffi.metatype(EyeBlink_t, {__index = EyeBlink})
	setmetatable(EyeBlink, {
		__call = function(_)
			return EyeBlink_t({
				eyeState = STATE_FIRST,
				blinkIntervalMsec = 4000,
				closingMotionMsec = 100,
				closedMotionMsec  =  50,
				openingMotionMsec = 150,
				closeIfZero = true,
			})
		end
	})
end

return EyeBlink
