local path = (...):sub(1, #(...) - #(".framework.MotionManager"))
local Kasumy = require(path..".dummy")

local MotionQueueManager = require(path..".motion.MotionQueueManager")

local MotionManager = Kasumy.Luaoop.class("Kasumy.Framework.MotionManager", MotionQueueManager)

function MotionManager:__construct()
	MotionQueueManager.__construct(self)
	self.currentPriority = 0
	self.reservePriority = 0
end

function MotionManager:getCurrentPriority()
	return self.currentPriority
end

function MotionManager:getReservePriority()
	return self.reservePriority
end

function MotionManager:setReservePriority(val)
	self.reservePriority = val
end

function MotionManager:startMotionPrio(motion, isDelete, priority)
	if priority == self.reservePriority then
		self.reservePriority = 0
	end

	self.currentPriority = priority
	return MotionQueueManager.startMotion(self, motion, isDelete)
end

function MotionManager:updateParam(model)
	local updated = MotionQueueManager.updateParam(self, model)
	if self:isFinished() then
		self.currentPriority = 0
	end

	return updated
end

function MotionManager:reserveMotion(priority)
	if priority <= self.reservePriority or priority <= self.currentPriority then
		return false
	end

	self.reservePriority = priority
	return true
end

return MotionManager
