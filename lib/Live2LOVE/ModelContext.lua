local path = (...):sub(1, #(...) - #(".ModelContext"))
local Kasumy = require(path..".dummy")

local IDrawData = require(path..".draw.IDrawData")

local ModelContext = Kasumy.Luaoop.class("Kasumy.Live2D.ModelContext")

local min = math.min
local max = math.max
local floor = math.floor

ModelContext._PARAM_FLOAT_MIN_ = -1000000
ModelContext._PARAM_FLOAT_MAX_ = 1000000
ModelContext.PARAM_NOT_UPDATED = false
ModelContext.PARAM_UPDATED = true
ModelContext.__HP = 0
ModelContext.__V2 = -1
ModelContext.__W0 = -1

function ModelContext:__construct(model)
	self.needSetup = true;
	self.initVersion = -1;
	self.floatParamCount = 0;
	self.floatParamIDList = {}
	self.floatParamList = {}
	self.lastFloatParamList = {}
	self.floatParamMinList = {}
	self.floatParamMaxList = {}
	self.savedFloatParamList = {}
	self.updatedFloatParamFlags = {}
	self.baseDataList = {}
	self.drawDataList = {}
	self.partsDataList = {}
	self.baseContextList = {}
	self.drawContextList = {}
	self.partsContextList = {}
	self.__Ws = nil
	self.__Vs = nil
	self.__Er = nil
	if Kasumy.ffi then
		self.tmpPivotTableIndices_short = Kasumy.ffi.new("double[65]")
		self.tmpT_array = Kasumy.ffi.new("double[11]")
	else
		self.tmpPivotTableIndices_short = {}
		self.tmpT_array = {}
	end
	self.model = model
	-- TODO: Make these thing for love.graphics
	ModelContext.__HP = ModelContext.__HP + 1
	self.__b0 = ModelContext.__HP
	self.clipManager = nil
	self.dp_webgl = nil
end

function ModelContext:getDrawDataIndex(id)
	for i = 1, #self.drawDataList do
		if self.drawDataList[i]:getDrawDataID() == id then
			return i
		end
	end

	return -1
end

function ModelContext:getDrawData(id)
	if type(id) == "string" then
		-- Use same list for string map too, thanks Lua
		-- But only do partial allocation
		for i = 1, #self.drawDataList do
			local ddid = self.drawDataList[i]:getDrawDataID()
			self.drawDataList[ddid] = self.drawDataList[i]
			if ddid == id then
				return self.drawDataList[i]
			end
		end
	else
		return self.drawDataList[id]
	end
end

function ModelContext:getDrawDataCount()
	return #self.drawDataList
end

function ModelContext:init()
	self.initVersion = self.initVersion + 1
	local modelImpl = self.model:getModelImpl()
	local partsDataList = modelImpl:getPartsDataList()
	local aH, a3 = {}, {}

	for i = 1, #partsDataList do
		local partsData = partsDataList[i]
		self.partsDataList[#self.partsDataList + 1] = partsData
		self.partsContextList[#self.partsContextList + 1] = partsData:init(self)

		local baseDataList = partsData:getBaseData()
		for j = 1, #baseDataList do
			local baseData = baseDataList[j]
			local context = baseData:init(self)
			context:setPartsIndex(i)
			aH[#aH + 1] = baseData
			a3[#a3 + 1] = context
		end

		local drawDataIndex = partsData:getDrawData()
		for j = 1, #drawDataIndex do
			local drawData = drawDataIndex[j]
			local context = drawData:init(self)
			context:setPartsIndex(i)
			self.drawDataList[#self.drawDataList + 1] = drawData
			self.drawContextList[#self.drawContextList + 1] = context
		end
	end

	local aY = #aH
	while true do
		local aX = false
		for i = 1, aY do
			local baseData = aH[i]
			if baseData then
				local targetID = baseData:getTargetBaseDataID()
				-- Lua is 1-based indexing, so check if it's bigger than 0
				if targetID == nil or targetID == "DST_BASE" or self:getBaseDataIndex(targetID) > 0 then
					self.baseDataList[#self.baseDataList + 1] = baseData
					self.baseContextList[#self.baseContextList + 1] = a3[i]
					aX = true
					aH[i] = nil
				end
			end
		end

		if not(aX) then break end
	end

	local paramDefSet = modelImpl:getParamDefSet()
	if paramDefSet then
		local paramDefList = paramDefSet:getParamDefFloatList()
		if paramDefList then
			for i = 1, #paramDefList do
				local paramDef = paramDefList[i]
				self:addFloatParam(
					paramDef:getParamID(),
					paramDef:getDefaultValue(),
					paramDef:getMinValue(),
					paramDef:getMaxValue()
				)
			end
		end
	end

	self.needSetup = true
end

function ModelContext:update()
	for i = 1, #self.floatParamList do
		if self.floatParamList[i] ~= self.lastFloatParamList[i] then
			self.updatedFloatParamFlags[i] = ModelContext.PARAM_UPDATED
			self.lastFloatParamList[i] = self.floatParamList[i]
		end
	end

	local minOrder = IDrawData.getTotalMinOrder()
	local maxOrder = IDrawData.getTotalMaxOrder()
	local aU = maxOrder - minOrder + 1

	if not(self.__Ws) then
		self.__Ws, self.__Vs = {}, {}
	end
	for i = 1, aU do
		self.__Ws[i], self.__Vs[i] = ModelContext.__V2, ModelContext.__V2
	end

	if not(self.__Er) then
		self.__Er = {}
	end
	for i = 1, #self.drawDataList do
		self.__Er[i] = ModelContext.__W0
	end

	for j = 1, #self.baseDataList do
		local a, b = self.baseDataList[j], self.baseContextList[j]
		a:setupInterpolate(self, b)
		a:setupTransform(self, b)
	end

	for k = 1, #self.drawDataList do
		local drawData, drawContext = self.drawDataList[k], self.drawContextList[k]
		drawData:setupInterpolate(self, drawContext)

		if not(drawContext:isParamOutside()) then
			drawData:setupTransform(self, drawContext)

			local aT = floor(drawData:getDrawOrder(self, drawContext) - minOrder) + 1
			local aP = self.__Vs[aT]
			if aP == ModelContext.__V2 then
				self.__Ws[aT] = k
			else
				self.__Er[aP] = k
			end
			self.__Vs[aT] = k
		end
	end

	for i = 1, #self.floatParamList do
		self.updatedFloatParamFlags[i] = ModelContext.PARAM_NOT_UPDATED
	end
	self.needSetup = false
	return false
end

function ModelContext.draw()
	-- TODO
end

function ModelContext:getParamIndex(id)
	for i = 1, #self.floatParamIDList do
		if self.floatParamIDList[i] == id then
			return i
		end
	end

	return self:addFloatParam(id, 0, ModelContext._PARAM_FLOAT_MIN_, ModelContext._PARAM_FLOAT_MAX_)
end

function ModelContext:getBaseDataIndex(id)
	for i = 1, #self.baseDataList do
		if self.baseDataList[i]:getBaseDataID() == id then
			return i
		end
	end

	return -1
end

function ModelContext:addFloatParam(id, value, minv, maxv)
	local i = #self.floatParamIDList + 1
	self.floatParamIDList[i] = id
	self.floatParamList[i] = value
	self.lastFloatParamList[i] = value
	self.floatParamMinList[i] = minv
	self.floatParamMaxList[i] = maxv
	self.updatedFloatParamFlags[i] = ModelContext.PARAM_UPDATED
	return i
end

function ModelContext:setBaseData(baseDataIndex, baseData)
	self.baseDataList[baseDataIndex] = baseData
end

function ModelContext:setParamFloat(paramIndex, value)
	value = min(self.floatParamMaxList[paramIndex], max(self.floatParamMinList[paramIndex], value))
	self.floatParamList[paramIndex] = value
end

function ModelContext:loadParam()
	for i = 1, #self.savedFloatParamList do
		self.floatParamList[i] = self.savedFloatParamList[i]
	end
end

function ModelContext:saveParam()
	for i = 1, #self.floatParamList do
		self.savedFloatParamList[i] = self.floatParamList[i]
	end
end

function ModelContext:getInitVersion()
	return self.initVersion
end

function ModelContext:requireSetup()
	return self.needSetup
end

function ModelContext:isParamUpdated(id)
	return self.updatedFloatParamFlags[id] == ModelContext.PARAM_UPDATED
end

function ModelContext:getTmpPivotTableIndicesRef()
	return self.tmpPivotTableIndices_short
end

function ModelContext:getTmpT_ArrayRef()
	return self.tmpT_array
end

function ModelContext:getBaseData(id)
	return self.baseDataList[id]
end

function ModelContext:getParamFloat(index)
	return self.floatParamList[index]
end

function ModelContext:getParamMax(index)
	return self.floatParamMaxList[index]
end

function ModelContext:getParamMin(index)
	return self.floatParamMinList[index]
end

function ModelContext:setPartsOpacity(index, opacity)
	self.partsContextList[index]:setPartsOpacity(opacity)
end

function ModelContext:getPartsOpacity(index)
	return self.partsContextList[index]:getPartsOpacity()
end

function ModelContext:getPartsDataIndex(id)
	for i = 1, #self.partsDataList do
		if self.partsDataList[i]:getPartsDataID() == id then
			return i
		end
	end

	return -1
end

function ModelContext:getBaseContext(index)
	return self.baseContextList[index]
end

function ModelContext:getDrawContext(index)
	return self.drawContextList[index]
end

function ModelContext:getPartsContext(index)
	return self.partsContextList[index]
end

return ModelContext
