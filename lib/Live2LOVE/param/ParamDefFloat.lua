local path = (...):sub(1, #(...) - #(".param.ParamDefFloat"))
local Kasumy = require(path..".dummy")

local ParamDefFloat = Kasumy.Luaoop.class("Kasumy.ParamDefFloat")

function ParamDefFloat:__construct()
	self.minValue, self.maxValue, self.defaultValue, self.paramID = nil, nil, nil, nil
end

function ParamDefFloat:readV2(br)
	self.minValue = br:readFloat()
	self.maxValue = br:readFloat()
	self.defaultValue = br:readFloat()
	self.paramID = br:readObject()
end

function ParamDefFloat:getMinValue()
	return self.minValue
end

function ParamDefFloat:getMaxValue()
	return self.maxValue
end

function ParamDefFloat:getDefaultValue()
	return self.defaultValue
end

function ParamDefFloat:getParamID()
	return self.paramID
end

return ParamDefFloat
