local path = (...):sub(1, #(...) - #(".param.ParamDefSet"))
local Kasumy = require(path..".dummy")

local ParamDefSet = Kasumy.Luaoop.class("Kasumy.ParamDefSet")

function ParamDefSet:__construct()
	self.paramDefList = nil
end

function ParamDefSet:getParamDefFloatList()
	return self.paramDefList
end

function ParamDefSet:initDirect()
	self.paramDefList = {}
end

function ParamDefSet:readV2(br)
	self.paramDefList = br:readObject()
end

function ParamDefSet:addParamDefFloat(x)
	self.paramDefList[#self.paramDefList + 1] = x
end

return ParamDefSet
