local path = (...):sub(1, #(...) - #(".param.PivotManager"))
local Kasumy = require(path..".dummy")

local DEF = require(path..".DEF")
local ParamPivots = require(path..".param.ParamPivots")

local PivotManager = Kasumy.Luaoop.class("Kasumy.PivotManager")

function PivotManager:__construct()
	self.paramPivotTable = nil
end

function PivotManager:readV2(br)
	self.paramPivotTable = br:readObject()
end

function PivotManager:initDirect()
	self.paramPivotTable = {}
end

function PivotManager:calcPivotValue(mdc)
	local ver = mdc:getInitVersion()
	local ret = 0
	local paramOutside = false

	for i = 1, #self.paramPivotTable do
		local paramPivot = self.paramPivotTable[i]
		local paramIndex = paramPivot:getParamIndex(ver)

		if paramIndex == ParamPivots.PARAM_INDEX_NOT_INIT then
			paramIndex = mdc:getParamIndex(paramPivot:getParamID());
			paramPivot:setParamIndex(paramIndex, ver);
		end

		if paramIndex < 0 then
			error("param index is negative "..paramIndex)
		end

		local paramValue = mdc:getParamFloat(paramIndex)
		local pivotCount = paramPivot:getPivotCount()
		local pivotValue = paramPivot:getPivotValue()
		local tmpPIdx = -1
		local tmpV = 0

		if pivotCount == 1 then
			local pv = pivotValue[1]
			if (pv - 0.0001 < paramValue) and (paramValue < pv + 0.0001) then
				tmpV = 0
			else
				paramOutside = true
			end

			tmpPIdx = 0
		elseif pivotCount > 1 then
			local pv = pivotValue[1]
			if paramValue < pv - 0.0001 then
				tmpPIdx = 0
				paramOutside = true
			elseif paramValue < pv + 0.0001 then
				tmpPIdx = 0
			else
				local found = false
				for j = 2, pivotCount do
					local pvi = pivotValue[j]
					if paramValue < pvi + 0.0001 then
						if pvi - 0.0001 < paramValue then
							tmpPIdx = j - 1
						else
							tmpPIdx = j - 2
							tmpV = (paramValue - pv) / (pvi - pv)
							ret = ret + 1
						end
						found = true
						break
					end
					pv = pvi
				end

				if not(found) then
					tmpPIdx = pivotCount - 1
					tmpV = 0
					paramOutside = true
				end
			end
		end

		paramPivot:setTmpPivotIndex(tmpPIdx);
		paramPivot:setTmpT(tmpV);
	end

	return ret, paramOutside
end

function PivotManager:calcPivotIndexies(array64, tmpT_array, interpolateCount)
	local alen = 2 ^ interpolateCount
	if alen + 1 > DEF.PIVOT_TABLE_SIZE then
		error("interpolate count exceeded pivot table size")
	end

	for i = 1, alen do
		array64[i] = 0
	end

	local k = 1
	local u = 1
	local p = 1
	for j = 1, #self.paramPivotTable do
		local paramPivot = self.paramPivotTable[j]
		local q = paramPivot:getTmpPivotIndex() * k

		if paramPivot:getTmpT() == 0 then
			if q < 0 then
				error("temp pivot index is less than 0")
			end

			for i = 1, alen do
				array64[i] = array64[i] + q
			end
		else
			local s = k * (paramPivot:getTmpPivotIndex() + 1)
			for i = 1, alen do
				array64[i] = array64[i] + (math.floor((i - 1) / u) % 2 == 0 and q or s)
			end

			tmpT_array[p] = paramPivot:getTmpT()
			p = p + 1
			u = u * 2
		end

		k = k * paramPivot:getPivotCount()
	end

	array64[alen + 1] = -1
	tmpT_array[p] = -1
end

function PivotManager:checkParamUpdated(mdc)
	if mdc:requireSetup() then
		return true
	end

	local initVersion = mdc:getInitVersion()
	for i = #self.paramPivotTable, 1, -1 do
		local pv = self.paramPivotTable[i]
		local pidx = pv:getParamIndex(initVersion)
		if pidx == ParamPivots.PARAM_INDEX_NOT_INIT then
			pidx = mdc:getParamIndex(pv:getParamID())
		end

		if mdc:isParamUpdated(pidx) then
			return true
		end
	end

	return false
end

function PivotManager:getParamCount()
	return #self.paramPivotTable
end

function PivotManager:getParamPivotTableRef()
	return self.paramPivotTable
end

return PivotManager
