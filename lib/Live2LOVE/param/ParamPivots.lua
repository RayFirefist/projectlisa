local path = (...):sub(1, #(...) - #(".param.ParamPivots"))
local Kasumy = require(path..".dummy")

local ParamPivots = Kasumy.Luaoop.class("Kasumy.ParamPivots")

ParamPivots.PARAM_INDEX_NOT_INIT = -2

function ParamPivots:__construct()
	self.pivotCount = 0
	self.paramID = nil
	self.pivotValue = nil
	self.paramIndex = ParamPivots.PARAM_INDEX_NOT_INIT
	self.indexInitVersion = -1
	self.tmpPivotIndex = 0
	self.tmpT = 0
end

function ParamPivots:readV2(br)
	self.paramID = br:readObject();
	self.pivotCount = br:readInt();
	self.pivotValue = br:readObject();

	if Kasumy.ffi then
		local val = Kasumy.ffi.new("double[?]", self.pivotCount + 1)
		for i = 1, self.pivotCount do
			val[i] = self.pivotValue[i]
		end
		self.pivotValue = val
	end
end

function ParamPivots:getParamIndex(initVersion)
	if self.indexInitVersion ~= initVersion then
		self.paramIndex = ParamPivots.PARAM_INDEX_NOT_INIT
	end

	return self.paramIndex
end

function ParamPivots:setParamIndex(index, initVersion)
	self.paramIndex = index
	self.indexInitVersion = initVersion
end

function ParamPivots:getParamID()
	return self.paramID
end

function ParamPivots:setParamID(v)
	self.paramID = v
end

function ParamPivots:getPivotCount()
	return self.pivotCount
end

function ParamPivots:getPivotValue()
	return self.pivotValue
end

function ParamPivots:setPivotValue(count, value)
	self.pivotCount = count

	if Kasumy.ffi then
		local val = Kasumy.ffi.new("double[?]", count + 1)
		for i = 1, count do
			val[i] = value[i]
		end
		self.pivotValue = val
	else
		self.pivotValue = value
	end
end

function ParamPivots:getTmpPivotIndex()
	return self.tmpPivotIndex
end

function ParamPivots:setTmpPivotIndex(v)
	self.tmpPivotIndex = v
end

function ParamPivots:getTmpT()
	return self.tmpT
end

function ParamPivots:setTmpT(v)
	self.tmpT = v
end

return ParamPivots
