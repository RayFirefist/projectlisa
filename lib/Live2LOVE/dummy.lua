local path = (...):sub(1, #(...) - #(".dummy"))

return {
	ffi = (jit and jit.status() and package.preload.ffi) and require("ffi"),
	JSON = require(path..".3p.JSON"),
	Luaoop = require(path..".3p.Luaoop")
}
