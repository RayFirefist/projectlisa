local love = require("love")
assert(love._version >= "11.0", "Kasumy require LOVE 11.0 or later")
assert(love.graphics, "Kasumy requires love.graphics to function")

local path = ...
local Kasumy = require(path..".dummy")

-- Live2D types
local AffineEnt = require(path..".base.AffineEnt")
local BDAffine = require(path..".base.BDAffine")
local BDBoxGrid = require(path..".base.BDBoxGrid")
local DDTexture = require(path..".draw.DDTexture")
local ModelImpl = require(path..".model.ModelImpl")
local PartsData = require(path..".model.PartsData")
local ParamDefFloat = require(path..".param.ParamDefFloat")
local ParamDefSet = require(path..".param.ParamDefSet")
local PivotManager = require(path..".param.PivotManager")
local ParamPivots = require(path..".param.ParamPivots")
local FileFormat2 = require(path..".io.FileFormat2")

-- Live2LOVE
Kasumy.Live2DModelLOVE = require(path..".Live2DModelLOVE")
local Live2LOVE = require(path..".Live2LOVE")

function FileFormat2.newInstance(id)
	if id == 65 then
		return BDBoxGrid()
	elseif id == 66 then
		return PivotManager()
	elseif id == 67 then
		return ParamPivots()
	elseif id == 68 then
		return BDAffine()
	elseif id == 69 then
		return AffineEnt()
	elseif id == 70 then
		return DDTexture()
	elseif id == 131 then
		return ParamDefFloat()
	elseif id == 133 then
		return PartsData()
	elseif id == 136 then
		return ModelImpl()
	elseif id == 137 then
		return ParamDefSet()
	elseif id == 142 then
		error("TODO: implement AvatarPartsItem")
		--return AvatarPartsItem()
	else
		error("invalid class number "..id)
	end
end

-- Live2LOVE compatible interface
function Kasumy.loadMocFile(f)
	return Live2LOVE(f)
end

local imageAttribute = {mipmaps = true}
function Kasumy.loadModel(file, setting)
	local dir = file:match("(.*[/])")
	-- Read model definition file
	local model = Kasumy.JSON:decode(love.filesystem.read(file))
	-- Open cubism model file
	local l2l = Live2LOVE(dir..model.model)
	-- Load physics file
	if model.physics then
		l2l:loadPhysics(dir..model.physics)
	end

	-- Set texture
	for i, v in ipairs(model.textures) do
		if v:sub(-4) ~= ".png" then v = v..".png" end
		local img = love.graphics.newImage(dir..v, setting or imageAttribute)
		img:setFilter("linear", "linear", 16)
		img:setMipmapFilter("linear", 16)
		l2l:setTexture(i, img)
	end

	-- Load expression
	local default
	if model.expressions then
		for _, v in ipairs(model.expressions) do
			if not(default) and v.name:find("default", 1, true) then default = v.name end
			l2l:loadExpression(v.name, dir..v.file)
		end
	end

	-- Load motion
	local idle
	if model.motions then
		for n, v in pairs(model.motions) do
			if #v > 1 then
				for j = 1, #v do
					local u = assert(v[j])
					local m = n..":"..j
					if not(idle) and m:find("idle", 1, true) == 1 then idle = m end
					l2l:loadMotion(m, u.fade_in or 1000, u.fade_out or 1000, dir..u.file)
				end
			else
				v = v[1]
				if not(idle) and n:find("idle", 1, true) == 1 then idle = n end
				l2l:loadMotion(n, v.fade_in or 1000, v.fade_out or 1000, dir..v.file)
			end
		end
	end

	-- Set default expression
	if default then l2l:setExpression(default) end
	-- Set to idle motion
	if idle then l2l:setMotion(idle, "loop") end

	return l2l
end

Kasumy._VERSION = "0.5.0"
Kasumy.Live2DVersion = "1.0.0 (Kasumy)"

return Kasumy
