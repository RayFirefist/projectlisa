-- Live2LOVE compatible API
local path = (...):sub(1, #(...) - #(".Live2LOVE"))

local love = require("love")

local Kasumy = require(path..".dummy")

local Live2DModelLOVE = require(path..".Live2DModelLOVE")
local DDTexture = require(path..".draw.DDTexture")
local MotionManager = require(path..".framework.MotionManager")
local Physics = require(path..".framework.Physics")
local ExpressionMotion = require(path..".framework.ExpressionMotion")
local EyeBlink = require(path..".framework.EyeBlink")
local Live2DMotion = require(path..".motion.Live2DMotion")
local UtSystem = require(path..".util.UtSystem")

local Live2LOVE = Kasumy.Luaoop.class("Kasumy.Live2LOVE")

local stencilTemp = {
	args = {},
	frag = nil,
	recursiveCount = 0
}

local function compareDrawOrder(a, b)
	local da = a.drawData:getDrawOrder(a.modelContext, a.drawContext)
	local db = b.drawData:getDrawOrder(b.modelContext, b.drawContext)

	if da == db then
		if a.partsIndex == b.partsIndex then
			return a.drawDataIndex < b.drawDataIndex
		else
			return a.partsIndex < b.partsIndex
		end
	else
		return da < db
	end
end

local newMeshData, updateMeshData

if Kasumy.ffi then
	local floor = math.floor
	local min = math.min
	local max = math.max

	if not(pcall(Kasumy.ffi.typeof, "KasumyVertexFmt")) then
		Kasumy.ffi.cdef[[
			typedef struct KasumyVertexFmt
			{
				float x, y, u, v;
				unsigned char r, g, b, a;
			} KasumyVertexFmt;
		]]
	end

	function newMeshData(n)
		local d = love.data.newByteData(Kasumy.ffi.sizeof("KasumyVertexFmt") * n)
		local p = Kasumy.ffi.cast("KasumyVertexFmt*", d:getPointer())
		return {d, p, n}
	end

	function updateMeshData(mesh, data, points, pointsLen, uv, opacity)
		opacity = opacity or 1
		for i = 1, pointsLen do
			local v = data[2][i - 1]
			v.x, v.y = assert(points[i * 2 - 1]), assert(points[i * 2 - 0])
			v.u, v.v = assert(uv[i * 2 - 1]), assert(uv[i * 2 - 0])
			v.r, v.g, v.b, v.a = 255, 255, 255, floor(min(max(opacity * 255, 0), 255))
		end

		mesh:setVertices(data[1])
	end
else
	function newMeshData(n)
		local t = {}
		for i = 1, n do
			t[i] = {0, 0, 0, 0, 1, 1, 1, 1}
		end

		return t
	end

	function updateMeshData(mesh, data, points, pointsLen, uv, opacity)
		opacity = opacity or 1
		for i = 1, pointsLen do
			local v = data[i]
			v[1], v[2] = assert(points[i * 2 - 1]), assert(points[i * 2 - 0])
			v[3], v[4] = assert(uv[i * 2 - 1]), assert(uv[i * 2 - 0])
			v[5], v[6], v[7], v[8] = 1, 1, 1, opacity
		end

		mesh:setVertices(data)
	end
end

function Live2LOVE:__construct(mocFileData)
	-- create stencil fragment shader
	if not(stencilTemp.frag) then
		stencilTemp.frag = love.graphics.newShader([[
			vec4 effect(vec4 color, Image tex, vec2 tc, vec2 sc)
			{
				if (Texel(tex, tc).a > 0.003) return vec4(1.0, 1.0, 1.0, 1.0);
				else discard;
			}
		]])
	end

	self.motion = nil
	self.expression = nil
	self.eyeBlink = EyeBlink()
	self.physics = nil

	self.meshData = {}
	self.motionList = {}
	self.expressionList = {}
	self.elapsedTime = 0 -- % 31536000
	self.movementAnimation = true
	self.eyeBlinkMovement = true
	self.motionLoop = ""
	self.paramUpdateList = {}
	self.paramUpdateNameList = {}
	self.model = Live2DModelLOVE.loadModel(mocFileData)
	self.model:update()
	return self:_setupMeshData()
end

function Live2LOVE:_setupMeshData()
	local modelContext = self.model:getModelContext()
	local drawableCount = modelContext:getDrawDataCount()

	-- Initialize mesh
	for i = 1, drawableCount do
		local ddtex = modelContext:getDrawData(i)
		local dctx = modelContext:getDrawContext(i)

		if Kasumy.Luaoop.class.is(ddtex, DDTexture) then
			local mesh = {
				drawData = ddtex,
				drawContext = dctx,
				modelContext = modelContext,
				drawDataIndex = i,
				partsIndex = dctx:getPartsIndex(),
				clipID = {}
			}
			mesh.partsContext = modelContext:getPartsContext(mesh.partsIndex)

			-- Create mesh table
			local vertexMap, mapLen = ddtex:getIndexArray()
			local points, pointsLen = mesh.drawContext:getTransformedPoints()
			local meshDataRaw = newMeshData(pointsLen)

			mesh.drawable = love.graphics.newMesh(pointsLen, "triangles", "stream")
			mesh.data = meshDataRaw

			if type(vertexMap) == "cdata" then
				-- Convert to temporary byte data
				local size = Kasumy.ffi.sizeof("uint16_t") * mapLen
				local temp = love.data.newByteData(size)
				Kasumy.ffi.copy(Kasumy.ffi.cast("void*", temp:getPointer()), vertexMap + 1, size)
				mesh.drawable:setVertexMap(temp, "uint16")
			else
				-- +1 index
				local temp = {}
				for j = 1, mapLen do
					temp[j] = vertexMap[j] + 1
				end
				mesh.drawable:setVertexMap(temp)
			end

			updateMeshData(
				mesh.drawable,
				meshDataRaw,
				points,
				pointsLen,
				mesh.drawData:getUvMap()
			)

			self.meshData[#self.meshData + 1] = mesh
			self.meshData[ddtex:getDrawDataID()] = mesh
		end
	end

	-- Find clippings
	for i = 1, drawableCount do
		local mesh = self.meshData[i]
		local clipList = mesh.drawData:getClipIDList()

		if clipList and #clipList > 0 then
			for j = 1, #clipList do
				mesh.clipID[#mesh.clipID + 1] = self.meshData[clipList[j]]
			end
		end
	end
end

function Live2LOVE:update(dt)
	self.elapsedTime = self.elapsedTime + dt
	UtSystem.setUserTimeMSec(self.elapsedTime * 1000)
	local t = self.elapsedTime * 2 * math.pi

	-- Update motion
	if self.motion then
		self.model:loadParam()

		if self.motion:isFinished() and self.motionLoop ~= "" then
			-- Revert
			self.motion:startMotion(self.motionList[self.motionLoop], false)
		end

		if
			self.motion:updateParam(self.model) == false and
			self.movementAnimation and
			self.eyeBlink and
			self.eyeBlinkMovement
		then
			self.eyeBlink:setParam(self.model)
		end

		self.model:saveParam()
	end

	-- Update expression
	if self.expression then self.expression:updateParam(self.model) end

	-- Movement animation update
	if self.movementAnimation then
		self.model:addToParamFloat("PARAM_ANGLE_X", 15.0 * math.sin(t / 6.5345), 0.5)
		self.model:addToParamFloat("PARAM_ANGLE_Y", 8.0 * math.sin(t / 3.5345), 0.5)
		self.model:addToParamFloat("PARAM_ANGLE_Z", 10.0 * math.sin(t / 5.5345), 0.5)
		self.model:addToParamFloat("PARAM_BODY_ANGLE_X", 4.0 * math.sin(t / 15.5345), 0.5)
		-- Override user-set value for PARAM_BREATH
		self.model:setParamFloat("PARAM_BREATH", 0.5 + 0.5 * math.sin(t / 3.2345), 1.0)

		-- Hair physics update
		if self.physics then self.physics:updateParam(self.model) end
	end

	for i = #self.paramUpdateNameList, 1, -1 do
		local name = self.paramUpdateNameList[i]
		local updateInfo = self.paramUpdateList[name]
		if updateInfo[1] == "set" then
			self.model:setParamFloat(name, updateInfo[2], updateInfo[3], updateInfo[4])
		elseif updateInfo[2] == "add" then
			self.model:addToParamFloat(name, updateInfo[2], updateInfo[3], updateInfo[4])
		elseif updateInfo[3] == "mul" then
			self.model:multParamFloat(name, updateInfo[2], updateInfo[3], updateInfo[4])
		end

		self.paramUpdateNameList[i] = nil
	end

	-- Update model
	self.model:update()

	-- Update mesh data
	for i = 1, #self.meshData do
		local mesh = self.meshData[i]

		local opacity =
			mesh.drawData:getOpacity(mesh.modelContext, mesh.drawContext) *
			mesh.drawContext.baseOpacity
		local points, pointsLen = mesh.drawContext:getTransformedPoints()
		updateMeshData(
			mesh.drawable,
			mesh.data,
			points,
			pointsLen,
			mesh.drawData:getUvMap(),
			opacity
		)
	end

	-- sort draw order
	table.sort(self.meshData, compareDrawOrder)
end

function stencilTemp.func2(list)
	local a = stencilTemp.args
	stencilTemp.recursiveCount = stencilTemp.recursiveCount + 1

	for i = 1, #list do
		local mesh = list[i]
		if #mesh.clipID > 0 then
			stencilTemp.func2(mesh.clipID)
		end

		love.graphics.draw(mesh.drawable, a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11])
	end
end

function stencilTemp.func()
	local shader = love.graphics.getShader()

	love.graphics.setShader(stencilTemp.frag)
	stencilTemp.func2(stencilTemp.args[1])
	love.graphics.setShader(shader)
end

function Live2LOVE:draw(...)
	local blendMode = DDTexture.COLOR_COMPOSITION_NORMAL
	local lastBlendMode, lastBlendAlpha = love.graphics.getBlendMode()

	love.graphics.setBlendMode("alpha", lastBlendAlpha)

	for i = 1, #self.meshData do
		local stencilSet = false
		local mesh = self.meshData[i]

		-- If there's clip, then draw it first with stencil
		if #mesh.clipID > 0 then
			local a = stencilTemp.args
			stencilTemp.recursiveCount = 0
			a[1] = mesh.clipID
			a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11] = ...
			love.graphics.stencil(stencilTemp.func, "increment")
			love.graphics.setStencilTest("gequal", stencilTemp.recursiveCount)
			stencilSet = true
		end

		local meshBlendMode = math.floor(mesh.drawData:getOptionFlag() / 2) % 8
		if meshBlendMode ~= blendMode then
			blendMode = meshBlendMode

			if blendMode == DDTexture.COLOR_COMPOSITION_NORMAL then
				-- Normal blending
				love.graphics.setBlendMode("alpha", lastBlendAlpha)
			elseif blendMode == DDTexture.COLOR_COMPOSITION_SCREEN then
				-- Screen blending
				love.graphics.setBlendMode("add", lastBlendAlpha)
			elseif blendMode == DDTexture.COLOR_COMPOSITION_MULTIPLY then
				-- Multiply blending
				love.graphics.setBlendMode("multiply", "premultiplied")
			end
		end

		love.graphics.draw(mesh.drawable, ...)

		if stencilSet then
			love.graphics.setStencilTest()
		end
	end

	love.graphics.setBlendMode(lastBlendMode, lastBlendAlpha)
end

function Live2LOVE:initializeMotion()
	if self.motion == nil then
		self.motion = MotionManager()
	end
end

function Live2LOVE:initializeExpression()
	if self.expression == nil then
		self.expression = MotionManager()
	end
end

function Live2LOVE:setTexture(texno, image)
	if type(image) == "userdata" and image.typeOf and image:typeOf("Texture") then
		for i = 1, #self.meshData do
			local mesh = self.meshData[i]
			if mesh.drawData:getTextureNo() == texno - 1 then
				mesh.drawable:setTexture(image)
			end
		end
	else
		error("bad argument #1 to 'setTexture' (Texture expected)")
	end
end

function Live2LOVE:setParamValue(name, value, weight)
	return self.model:setParamFloat(name, value, weight or 1)
end

function Live2LOVE:setParamValuePost(name, value, weight)
	self.paramUpdateList[name] = {"set", value, weight or 1}
	self.paramUpdateNameList[#self.paramUpdateNameList + 1] = name
end

function Live2LOVE:addParamValue(name, value, weight)
	return self.model:addToParamFloat(name, value, weight or 1)
end

function Live2LOVE:mulParamValue(name, value, weight)
	return self.model:multParamFloat(name, value, weight or 1)
end

function Live2LOVE:getParamValue(name)
	return self.model:getParamFloat(name)
end

function Live2LOVE:getParamInfoList()
	local ret = {}
	local x = self.model:getModelImpl():getParamDefSet():getParamDefFloatList()
	for i = 1, #x do
		local y = x[i]
		ret[i] = {
			name = y:getParamID(),
			min = y:getMinValue(),
			max = y:getMaxValue(),
			default = y:getDefaultValue()
		}
	end

	return ret
end

function Live2LOVE:setMotion(name, mode)
	assert(self.motion, "no motion loaded")

	if not(name) then
		self.motionLoop = ""
		return self.motion:stopAllMotions()
	else
		local mot = assert(self.motionList[name], "motion not found")

		if mode == "loop" then
			self.motionLoop = name
		elseif mode == "preserve" then
			self.motionLoop = ""
		elseif mode ~= "normal" then
			error("invalid motion mode")
		end

		return self.motion:startMotion(mot, false)
	end
end

function Live2LOVE:setAnimationMovement(a)
	self.movementAnimation = not(not(a))
end

function Live2LOVE:setEyeBlinkMovement(a)
	self.eyeBlinkMovement = not(not(a))
end

function Live2LOVE:loadMotion(name, fadeIn, fadeOut, file)
	self:initializeMotion()

	local motion = Live2DMotion.loadMotion(file)
	motion:setFadeIn(fadeIn or 1000)
	motion:setFadeOut(fadeOut or 1000)
	self.motionList[name] = motion
end

function Live2LOVE:setExpression(name)
	assert(self.expression, "no expression loaded")
	self.expression:startMotion(assert(self.expressionList[name], "expression not found"), false)
end

function Live2LOVE:loadExpression(name, file)
	self:initializeExpression()

	local expr = ExpressionMotion.loadJson(file)
	self.expressionList[name] = expr
end

function Live2LOVE:loadPhysics(file)
	self.physics = Physics.load(file)
end

function Live2LOVE:getMeshCount()
	return #self.meshData
end

function Live2LOVE:getMesh(index)
	if index == nil then
		-- All mesh
		local ret = {}
		for i = 1, #self.meshData do
			ret[#ret + 1] = self.meshData[i].drawable
		end
		return ret
	else
		-- Individual mesh
		return self.meshData[index].drawable
	end
end

function Live2LOVE:getDimensions()
	return self.model:getCanvasWidth(), self.model:getCanvasHeight()
end

function Live2LOVE:getWidth()
	return self.model:getCanvasWidth()
end

function Live2LOVE:getHeight()
	return self.model:getCanvasHeight()
end

function Live2LOVE:getExpressionList()
	local ret = {}
	for n in pairs(self.expressionList) do
		ret[#ret + 1] = n
	end

	return ret
end

function Live2LOVE:getMotionList()
	local ret = {}
	for n in pairs(self.motionList) do
		ret[#ret + 1] = n
	end

	return ret
end

function Live2LOVE:isAnimationMovementEnabled()
	return self.movementAnimation
end

function Live2LOVE:isEyeBlinkEnabled()
	return self.eyeBlinkMovement
end

return Live2LOVE
