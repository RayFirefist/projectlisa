local path = (...):sub(1, #(...) - #(".base.BDAffine"))
local Kasumy = require(path..".dummy")

local AffineEnt = require(path..".base.AffineEnt")
local IBaseData = require(path..".base.IBaseData")
local IBaseContext = require(path..".base.IBaseContext")
local PivotManager = require(path..".param.PivotManager")

local BDAffine = Kasumy.Luaoop.class("Kasumy.BDAffine", IBaseData)
local BDAffineContext = Kasumy.Luaoop.class("Kasumy.BDAffineContext", IBaseContext)

IBaseData.TYPE_BD_AFFINE = BDAffine

local pi = math.pi
local atan2 = math.atan2 or math.atan
local sin = math.sin
local cos = math.cos

local newTempPoint
if Kasumy.ffi then
	function newTempPoint()
		return Kasumy.ffi.new("double[3]")
	end
else
	function newTempPoint()
		return {0, 0}
	end
end

-- I hope LuaJIT inline this lmao
local function lerp(v0, v1, t)
	return (1 - t) * v0 + t * v1
end

local function getAngleNotAbs(p1, p2)
	local ad = atan2(p1[2], p1[1]) - atan2(p2[2], p2[1])
	-- Thanks to Tjakka5 for this simpler variant
	return (ad + pi) % (2 * pi) - pi
end

-- BDAffineContext
function BDAffineContext:__construct(src)
	IBaseContext.__construct(self, src)
	self.tmpBaseDataIndex = IBaseData.BASE_INDEX_NOT_INIT
	self.interpolatedAffine = nil
	self.transformedAffine = nil
end

-- BDAffine
function BDAffine:__construct()
	IBaseData.__construct(self)
	self.pivotManager = nil
	self.affines = nil
end

function BDAffine:initDirect()
	self.pivotManager = PivotManager()
	self.pivotManager:initDirect()
	self.affines = {}
end

function BDAffine.getType()
	return BDAffine
end

function BDAffine:readV2(br)
	IBaseData.readV2(self, br)
	self.pivotManager = br:readObject()
	self.affines = br:readObject()
	IBaseData.readV2_opacity(self, br)
end

function BDAffine:init()
	local context = BDAffineContext(self)

	context.interpolatedAffine = AffineEnt()
	if self:needTransform() then
		context.transformedAffine = AffineEnt()
	end

	return context
end

function BDAffine:setupInterpolate(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, BDAffineContext), "cdata is not BDAffineContext")
	assert(cdata:getSrcPtr() == self, "invalid IBaseContext")

	if self.pivotManager:checkParamUpdated(mdc) then
		local pivotValue, paramOutside = self.pivotManager:calcPivotValue(mdc)
		cdata:setOutsideParam(paramOutside)
		self:interpolateOpacity(mdc, self.pivotManager, cdata)
		local a3 = mdc:getTmpPivotTableIndicesRef()
		local ba = mdc:getTmpT_ArrayRef()
		local ia = cdata.interpolatedAffine
		self.pivotManager:calcPivotIndexies(a3, ba, pivotValue)

		if pivotValue <= 0 then
			ia:init(self.affines[a3[1] + 1])
		elseif pivotValue == 1 then
			local bn = self.affines[a3[1] + 1]
			local bl = self.affines[a3[2] + 1]
			local a9 = ba[1]
			ia.originX = lerp(bn.originX, bl.originX, a9)
			ia.originY = lerp(bn.originY, bl.originY, a9)
			ia.scaleX = lerp(bn.scaleX, bl.scaleX, a9)
			ia.scaleY = lerp(bn.scaleY, bl.scaleY, a9)
			ia.rotate = lerp(bn.rotate, bl.rotate, a9)
		elseif pivotValue == 2 then
			local bn = self.affines[a3[1] + 1]
			local bl = self.affines[a3[2] + 1]
			local a1 = self.affines[a3[3] + 1]
			local a0 = self.affines[a3[4] + 1]
			local a9 = ba[1]
			local a8 = ba[2]
			ia.originX = lerp(lerp(bn.originX, bl.originX, a9), lerp(a1.originX, a0.originX, a9), a8)
			ia.originY = lerp(lerp(bn.originY, bl.originY, a9), lerp(a1.originY, a0.originY, a9), a8)
			ia.scaleX = lerp(lerp(bn.scaleX, bl.scaleX, a9), lerp(a1.scaleX, a0.scaleX, a9), a8)
			ia.scaleY = lerp(lerp(bn.scaleY, bl.scaleY, a9), lerp(a1.scaleY, a0.scaleY, a9), a8)
			ia.rotate = lerp(lerp(bn.rotate, bl.rotate, a9), lerp(a1.rotate, a0.rotate, a9), a8)
		elseif pivotValue == 3 then
			local aP = self.affines[a3[1] + 1]
			local aO = self.affines[a3[2] + 1]
			local bu = self.affines[a3[3] + 1]
			local bs = self.affines[a3[4] + 1]
			local aK = self.affines[a3[5] + 1]
			local aJ = self.affines[a3[6] + 1]
			local bj = self.affines[a3[7] + 1]
			local bi = self.affines[a3[8] + 1]
			local a9 = ba[1]
			local a8 = ba[2]
			local a6 = ba[3]
			ia.originX = lerp(
				lerp(lerp(aP.originX, aO.originX, a9), lerp(bu.originX, bs.originX, a9), a8),
				lerp(lerp(aK.originX, aJ.originX, a9), lerp(bj.originX, bi.originX, a9), a8),
				a6
			)
			ia.originY = lerp(
				lerp(lerp(aP.originY, aO.originY, a9), lerp(bu.originY, bs.originY, a9), a8),
				lerp(lerp(aK.originY, aJ.originY, a9), lerp(bj.originY, bi.originY, a9), a8),
				a6
			)
			ia.scaleX = lerp(
				lerp(lerp(aP.scaleX, aO.scaleX, a9), lerp(bu.scaleX, bs.scaleX, a9), a8),
				lerp(lerp(aK.scaleX, aJ.scaleX, a9), lerp(bj.scaleX, bi.scaleX, a9), a8),
				a6
			)
			ia.scaleY = lerp(
				lerp(lerp(aP.scaleY, aO.scaleY, a9), lerp(bu.scaleY, bs.scaleY, a9), a8),
				lerp(lerp(aK.scaleY, aJ.scaleY, a9), lerp(bj.scaleY, bi.scaleY, a9), a8),
				a6
			)
			ia.rotate = lerp(
				lerp(lerp(aP.rotate, aO.rotate, a9), lerp(bu.rotate, bs.rotate, a9), a8),
				lerp(lerp(aK.rotate, aJ.rotate, a9), lerp(bj.rotate, bi.rotate, a9), a8),
				a6
			)
		elseif pivotValue == 4 then
			local aT = self.affines[a3[1] + 1]
			local aS = self.affines[a3[2] + 1]
			local bE = self.affines[a3[3] + 1]
			local bD = self.affines[a3[4] + 1]
			local aN = self.affines[a3[5] + 1]
			local aM = self.affines[a3[6] + 1]
			local bp = self.affines[a3[7] + 1]
			local bo = self.affines[a3[8] + 1]
			local bh = self.affines[a3[9] + 1]
			local bg = self.affines[a3[10] + 1]
			local aY = self.affines[a3[11] + 1]
			local aW = self.affines[a3[12] + 1]
			local a7 = self.affines[a3[13] + 1]
			local a5 = self.affines[a3[14] + 1]
			local aR = self.affines[a3[15] + 1]
			local aQ = self.affines[a3[16] + 1]
			local a9 = ba[1]
			local a8 = ba[2]
			local a6 = ba[3]
			local a4 = ba[4]
			ia.originX = lerp(
				lerp(
					lerp(lerp(aT.originX, aS.originX, a9), lerp(bE.originX, bD.originX, a9), a8),
					lerp(lerp(aN.originX, aM.originX, a9), lerp(bp.originX, bo.originX, a9), a8),
					a6
				),
				lerp(
					lerp(lerp(bh.originX, bg.originX, a9), lerp(aY.originX, aW.originX, a9), a8),
					lerp(lerp(a7.originX, a5.originX, a9), lerp(aR.originX, aQ.originX, a9), a8),
					a6
				),
				a4
			)
			ia.originY = lerp(
				lerp(
					lerp(lerp(aT.originY, aS.originY, a9), lerp(bE.originY, bD.originY, a9), a8),
					lerp(lerp(aN.originY, aM.originY, a9), lerp(bp.originY, bo.originY, a9), a8),
					a6
				),
				lerp(
					lerp(lerp(bh.originY, bg.originY, a9), lerp(aY.originY, aW.originY, a9), a8),
					lerp(lerp(a7.originY, a5.originY, a9), lerp(aR.originY, aQ.originY, a9), a8),
					a6
				),
				a4
			)
			ia.scaleX = lerp(
				lerp(
					lerp(lerp(aT.scaleX, aS.scaleX, a9), lerp(bE.scaleX, bD.scaleX, a9), a8),
					lerp(lerp(aN.scaleX, aM.scaleX, a9), lerp(bp.scaleX, bo.scaleX, a9), a8),
					a6
				),
				lerp(
					lerp(lerp(bh.scaleX, bg.scaleX, a9), lerp(aY.scaleX, aW.scaleX, a9), a8),
					lerp(lerp(a7.scaleX, a5.scaleX, a9), lerp(aR.scaleX, aQ.scaleX, a9), a8),
					a6
				),
				a4
			)
			ia.scaleY = lerp(
				lerp(
					lerp(lerp(aT.scaleY, aS.scaleY, a9), lerp(bE.scaleY, bD.scaleY, a9), a8),
					lerp(lerp(aN.scaleY, aM.scaleY, a9), lerp(bp.scaleY, bo.scaleY, a9), a8),
					a6
				),
				lerp(
					lerp(lerp(bh.scaleY, bg.scaleY, a9), lerp(aY.scaleY, aW.scaleY, a9), a8),
					lerp(lerp(a7.scaleY, a5.scaleY, a9), lerp(aR.scaleY, aQ.scaleY, a9), a8),
					a6
				),
				a4
			)
			ia.rotate = lerp(
				lerp(
					lerp(lerp(aT.rotate, aS.rotate, a9), lerp(bE.rotate, bD.rotate, a9), a8),
					lerp(lerp(aN.rotate, aM.rotate, a9), lerp(bp.rotate, bo.rotate, a9), a8),
					a6
				),
				lerp(
					lerp(lerp(bh.rotate, bg.rotate, a9), lerp(aY.rotate, aW.rotate, a9), a8),
					lerp(lerp(a7.rotate, a5.rotate, a9), lerp(aR.rotate, aQ.rotate, a9), a8),
					a6
				),
				a4
			)
		else
			local aV = 2 ^ pivotValue
			local aZ = {}
			for i = 0, aV - 1 do
				local aI = i
				local aH = 1
				for j = 1, pivotValue do
					aH = aH * (aI % 2 == 0 and (1 - ba[j]) or ba[j])
					aI = aI / 2
				end
				aZ[i + 1] = aH
			end

			local be, bc, bd, bb, aX = 0, 0, 0, 0, 0
			for i = 1, aV do
				local bA = self.affines[a3[i] + 1]
				be = be + aZ[i] * bA.originX
				bc = bc + aZ[i] * bA.originY
				bd = bd + aZ[i] * bA.scaleX
				bb = bb + aZ[i] * bA.scaleY
				aX = aX + aZ[i] * bA.rotate
			end
			ia.originX, ia.originY = be, bc
			ia.scaleX, ia.scaleY = bd, bb
			ia.rotate = aX
		end

		local bn = self.affines[a3[1] + 1]
		ia.reflectX = bn.reflectX
		ia.reflectY = bn.reflectY
	end
end

local tempDirSrcOrig = newTempPoint()
local tempDirSrc = newTempPoint()
local tempDirRet = newTempPoint()

function BDAffine:setupTransform(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, BDAffineContext), "cdata is not BDAffineContext")
	assert(cdata:getSrcPtr() == self, "invalid IBaseContext")

	local ia = cdata.interpolatedAffine
	cdata:setAvailable(true)

	if self:needTransform() then
		if cdata.tmpBaseDataIndex == IBaseData.BASE_INDEX_NOT_INIT then
			cdata.tmpBaseDataIndex = mdc:getBaseDataIndex(self:getTargetBaseDataID())
		end

		if cdata.tmpBaseDataIndex < 0 then
			error("temp base data index is negative")
			--cdata:setAvailable(false)
		else
			local aI = mdc:getBaseData(cdata.tmpBaseDataIndex)
			if aI then
				local aL = mdc:getBaseContext(cdata.tmpBaseDataIndex)
				local aS = tempDirSrcOrig
				local aJ = tempDirSrc
				local aQ = tempDirRet
				local ta = cdata.transformedAffine
				aS[1] = ia.originX
				aS[2] = ia.originY
				aJ[1], aJ[2] = 0, (aI:getType() == BDAffine and -10 or -0.1)
				BDAffine.getDirectionOnDst(mdc, aI, aL, aS, aJ, aQ)
				local aP = getAngleNotAbs(aJ, aQ)
				aI:transformPoints(mdc, aL, aS, aS, 1, 0, 2)
				ta.originX = aS[1]
				ta.originY = aS[2]
				ta.scaleX = ia.scaleX
				ta.scaleY = ia.scaleY
				ta.rotate = ia.rotate - aP
				ta.reflectX = ia.reflectX
				ta.reflectY = ia.reflectY
				cdata:setTotalScale_notForClient(aL:getTotalScale() * ta.scaleX)
				cdata:setTotalOpacity(aL:getTotalOpacity() * cdata:getInterpolatedOpacity())
				cdata:setAvailable(aL:isAvailable())
			else
				cdata:setAvailable(false)
			end
		end
	else
		cdata:setTotalScale_notForClient(cdata.interpolatedAffine.scaleX)
		cdata:setTotalOpacity(cdata:getInterpolatedOpacity())
	end
end

function BDAffine:transformPoints(_, cdata, srcPoints, dstPoints, numPoint)
	assert(Kasumy.Luaoop.class.is(cdata, BDAffineContext), "cdata is not BDAffineContext")
	assert(cdata:getSrcPtr() == self, "invalid IBaseContext")

	local aff = cdata.transformedAffine or cdata.interpolatedAffine
	local s = sin(aff.rotate)
	local c = cos(aff.rotate)
	local scale = cdata:getTotalScale()
	local reflectX = aff.reflectX and -1 or 1
	local reflectY = aff.reflectY and -1 or 1
	local aS = c * scale * reflectX
	local aQ = -s * scale * reflectY
	local a1 = s * scale * reflectX
	local aZ = c * scale * reflectY
	local aY = aff.originX
	local aX = aff.originY
	for i = 1, numPoint * 2, 2 do
		local aN, aM = srcPoints[i], srcPoints[i + 1]
		dstPoints[i] = aS * aN + aQ * aM + aY
		dstPoints[i + 1] = a1 * aN + aZ * aM + aX
	end
end

local directionDstSrc = newTempPoint()
local directionDstDst = newTempPoint()
local directionDstTmp = newTempPoint()

function BDAffine.getDirectionOnDst(mdc, targetToDst, targetToDstContext, srcOrigin, srcDir, retDir)
	assert(targetToDstContext:getSrcPtr() == targetToDst, "invalid target IBaseContext")

	directionDstTmp[1], directionDstTmp[2] = srcOrigin[1], srcOrigin[2]
	targetToDst:transformPoints(mdc, targetToDstContext, directionDstTmp, directionDstTmp, 1, 0, 2)

	local aJ = 1
	for _ = 1, 10 do
		directionDstDst[1] = srcOrigin[1] + aJ * srcDir[1]
		directionDstDst[2] = srcOrigin[2] + aJ * srcDir[2]
		targetToDst:transformPoints(mdc, targetToDstContext, directionDstDst, directionDstSrc, 1, 0, 2)
		directionDstSrc[1] = directionDstSrc[1] - directionDstTmp[1]
		directionDstSrc[2] = directionDstSrc[2] - directionDstTmp[2]
		if directionDstSrc[1] ~= 0 or directionDstSrc[2] ~= 0 then
			retDir[1] = directionDstSrc[1]
			retDir[2] = directionDstSrc[2]
			return
		end
		directionDstDst[1] = srcOrigin[1] - aJ * srcDir[1]
		directionDstDst[2] = srcOrigin[2] - aJ * srcDir[2]
		targetToDst:transformPoints(mdc, targetToDstContext, directionDstDst, directionDstSrc, 1, 0, 2)
		directionDstSrc[1] = directionDstSrc[1] - directionDstTmp[1]
		directionDstSrc[2] = directionDstSrc[2] - directionDstTmp[2]
		if directionDstSrc[1] ~= 0 or directionDstSrc[2] ~= 0 then
			directionDstSrc[1] = -directionDstSrc[1]
			--directionDstSrc[1] = -directionDstSrc[1] -- I consider this line as bug
			directionDstSrc[2] = -directionDstSrc[2]
			retDir[1] = directionDstSrc[1]
			retDir[2] = directionDstSrc[2]
			return
		end
		aJ = aJ * 0.1
	end

	error("getDirectionOnDst failed")
end

return BDAffine
