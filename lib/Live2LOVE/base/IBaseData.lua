local path = (...):sub(1, #(...) - #(".base.IBaseData"))
local Kasumy = require(path..".dummy")

local UtInterpolate = require(path..".util.UtInterpolate")
local FileFormat2 = require(path..".io.FileFormat2")
local IBaseData = Kasumy.Luaoop.class("Kasumy.IBaseData")

IBaseData.BASE_INDEX_NOT_INIT = -2
IBaseData.TYPE_BD_AFFINE = nil   -- set by BDAffine.lua
IBaseData.TYPE_BD_BOX_GRID = nil -- set by BDBoxGrid.lua

function IBaseData:__construct()
	self.baseDataID = nil
	self.targetBaseDataID = nil
	self.dirty = true
	self.pivotOpacity = nil
end

function IBaseData:setTargetBaseDataID(id)
	self.targetBaseDataID = id
end

function IBaseData:setBaseDataID(id)
	self.baseDataID = id
end

function IBaseData:getTargetBaseDataID()
	return self.targetBaseDataID
end

function IBaseData:getBaseDataID()
	return self.baseDataID
end

function IBaseData:needTransform()
	return self.targetBaseDataID and self.targetBaseDataID ~= "DST_BASE"
end

function IBaseData.init()
	error("pure virtual method 'IBaseData:init'")
end

function IBaseData:readV2(br)
	self.baseDataID = br:readObject()
	self.targetBaseDataID = br:readObject()
end

function IBaseData.setupInterpolate()
	error("pure virtual method 'IBaseData:setupInterpolate'")
end

function IBaseData.setupTransform()
	error("pure virtual method 'IBaseData:setupTransform'")
end

function IBaseData.transformPoints()
	error("pure virtual method 'IBaseData:transformPoints'")
end

function IBaseData.getType()
	error("pure virtual method 'IBaseData:getType'")
end

function IBaseData.dump()
	--error("pure virtual method 'IBaseData:dump'")
end

function IBaseData:readV2_opacity(br)
	if br:getFormatVersion() >= FileFormat2.LIVE2D_FORMAT_VERSION_V2_10_SDK2 then
		self.pivotOpacity = br:readArrayFloat()

		if Kasumy.ffi then
			self.pivotOpacity[0] = #self.pivotOpacity
			self.pivotOpacity = Kasumy.ffi.new("double[?]", #self.pivotOpacity + 1, self.pivotOpacity)
		end
	end
end

function IBaseData:interpolateOpacity(mdc, pivotMgr, _data)
	local interpolateValue = 1
	local paramOutside = false
	if self.pivotOpacity then
		interpolateValue, paramOutside = UtInterpolate.interpolateFloat(mdc, pivotMgr, self.pivotOpacity)
	end

	_data:setInterpolatedOpacity(assert(interpolateValue))
	return paramOutside
end

return IBaseData
