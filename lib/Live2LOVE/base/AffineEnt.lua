local path = (...):sub(1, #(...) - #(".base.AffineEnt"))
local Kasumy = require(path..".dummy")

local FileFormat2 = require(path..".io.FileFormat2")

local AffineEnt
local AffineEnt_t = nil

if Kasumy.ffi then
	AffineEnt_t = Kasumy.ffi.typeof([[struct {
		double originX;
		double originY;
		double scaleX;
		double scaleY;
		double rotate;
		bool reflectX,reflectY;
	}]])
	AffineEnt = {}
else
	AffineEnt = Kasumy.Luaoop.class("Kasumy.AffineEnt")

	function AffineEnt:__construct()
		self.originX = 0
		self.originY = 0
		self.scaleX = 1
		self.scaleY = 1
		self.rotate = 0
		self.reflectX = false
		self.reflectY = false
	end
end

function AffineEnt:init(t)
	self.originX = t.originX
	self.originY = t.originY
	self.scaleX = t.scaleX
	self.scaleY = t.scaleY
	self.rotate = t.rotate
	self.reflectX = t.reflectX
	self.reflectY = t.reflectY
end

function AffineEnt:readV2(br)
	self.originX = br:readFloat()
	self.originY = br:readFloat()
	self.scaleX = br:readFloat()
	self.scaleY = br:readFloat()
	self.rotate = math.rad(br:readFloat())
	if br:getFormatVersion() >= FileFormat2.LIVE2D_FORMAT_VERSION_V2_10_SDK2 then
		self.reflectX = br:readBoolean()
		self.reflectY = br:readBoolean()
	end
end

if AffineEnt_t then
	Kasumy.ffi.metatype(AffineEnt_t, {__index = AffineEnt})
	setmetatable(AffineEnt, {
		__call = function(_)
			return AffineEnt_t({
				originX = 0,
				originY = 0,
				scaleX = 1,
				scaleY = 1,
				rotate = 0,
				reflectX = false,
				reflectY = false
			})
		end
	})
end

return AffineEnt
