local path = (...):sub(1, #(...) - #(".base.BDBoxGrid"))
local Kasumy = require(path..".dummy")

local IBaseData = require(path..".base.IBaseData")
local IBaseContext = require(path..".base.IBaseContext")
local PivotManager = require(path..".param.PivotManager")
local UtInterpolate = require(path..".util.UtInterpolate")

local BDBoxGrid = Kasumy.Luaoop.class("Kasumy.BDBoxGrid", IBaseData)
local BDBoxGridContext = Kasumy.Luaoop.class("Kasumy.BDBoxGridContext", IBaseContext)

IBaseData.TYPE_BD_BOX_GRID = BDBoxGrid

local floor = math.floor
local newDouble

if Kasumy.ffi then
	function newDouble(n)
		return Kasumy.ffi.new("double[?]", n + 1)
	end
else
	function newDouble(n)
		local temp = {}
		for i = 1, n do temp[i] = 0 end
		return temp
	end
end

local function interp(a, b, c, t, u)
	return a + (b - a) * t + (c - a) * u
end

-- BDBoxGridContext
function BDBoxGridContext:__construct(src)
	IBaseContext.__construct(self, src)
	self.tmpBaseDataIndex = IBaseData.BASE_INDEX_NOT_INIT
	self.interpolatedPoints = nil
	self.transformedPoints = nil
end

-- BDBoxGrid
function BDBoxGrid:__construct()
	self.col = 0
	self.row = 0
	self.pivotManager = nil
	self.pivotPoints = nil
end

function BDBoxGrid:initDirect()
	self.pivotManager = PivotManager()
	self.pivotManager.initDirect()
	self.pivotPoints = {}
end

function BDBoxGrid:readV2(br)
	IBaseData.readV2(self, br)
	self.row = br:readInt()
	self.col = br:readInt()
	self.pivotManager = br:readObject()
	self.pivotPoints = br:readObject()

	if Kasumy.ffi then
		for i = 1, #self.pivotPoints do
			local c = self.pivotPoints[i]
			self.pivotPoints[i][0] = #c
			self.pivotPoints[i] = Kasumy.ffi.new("double[?]", #c + 1, c)
		end
	end

	IBaseData.readV2_opacity(self, br)
end

function BDBoxGrid:init()
	local context = BDBoxGridContext(self)
	local len = self:getNumPts() * 2
	context.interpolatedPoints = newDouble(len)

	if context.transformedPoints then
		context.transformedPoints = nil
	end

	if self:needTransform() then
		context.transformedPoints = newDouble(len)
	end

	return context
end

function BDBoxGrid:setupInterpolate(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, BDBoxGridContext), "cdata is not BDBoxGridContext")
	assert(cdata:getSrcPtr() == self, "invalid IBaseContext")

	if self.pivotManager:checkParamUpdated(mdc) then
		cdata:setOutsideParam(UtInterpolate.interpolatePoints(
			mdc,
			self.pivotManager,
			self:getNumPts(),
			self.pivotPoints,
			cdata.interpolatedPoints,
			0, 2
		))
		self:interpolateOpacity(mdc, self.pivotManager, cdata)
	end
end

function BDBoxGrid:setupTransform(mdc, cdata)
	assert(Kasumy.Luaoop.class.is(cdata, BDBoxGridContext), "cdata is not BDBoxGridContext")
	assert(cdata:getSrcPtr() == self, "invalid IBaseContext")

	cdata:setAvailable(true)
	if self:needTransform() then
		local target = self:getTargetBaseDataID()
		if cdata.tmpBaseDataIndex == IBaseData.BASE_INDEX_NOT_INIT then
			cdata.tmpBaseDataIndex = mdc:getBaseDataIndex(target)
		end

		if cdata.tmpBaseDataIndex < 0 then
			error("temp base data index is negative")
			--cdata:setAvailable(false)
		else
			local aN = mdc:getBaseData(cdata.tmpBaseDataIndex)
			local aI = mdc:getBaseContext(cdata.tmpBaseDataIndex)

			if aN and aI:isAvailable() then
				cdata:setTotalScale_notForClient(aI:getTotalScale())
				cdata:setTotalOpacity(cdata:getInterpolatedOpacity() * aI:getTotalOpacity())
				aN:transformPoints(mdc, aI, cdata.interpolatedPoints, cdata.transformedPoints, self:getNumPts(), 0, 2)
			else
				cdata:setAvailable(false)
			end
		end
	else
		cdata:setTotalOpacity(cdata:getInterpolatedOpacity())
	end
end

function BDBoxGrid:transformPoints(_, cdata, src, dst, numPoint)
	local pt = cdata.transformedPoints or cdata.interpolatedPoints
	local xSum = 0
	local ySum = 0
	local xDiffSum = 0
	local yDiffSum = 0
	local xDiffDiff = 0
	local yDiffDiff = 0
	local dataInit = false
	local col, row = self.col, self.row

	for i = 1, numPoint * 2, 2 do
		local x, y = src[i], src[i + 1]
		local bd = x * col
		local a7 = y * row

		if bd < 0 or a7 < 0 or col <= bd or row <= a7 then
			local wP1 = col + 1

			if not(dataInit) then
				dataInit = true
				xSum = 0.25 * (pt[1] + pt[col * 2 + 1] + pt[row * wP1 * 2 + 1] + pt[(col + row * wP1) * 2 + 1])
				ySum = 0.25 * (pt[2] + pt[col * 2 + 2] + pt[row * wP1 * 2 + 2] + pt[(col + row * wP1) * 2 + 2])
				local xTopRLdiff = pt[(col + row * wP1) * 2 + 1] - pt[1]
				local yTopRLdiff = pt[(col + row * wP1) * 2 + 2] - pt[2]
				local xRightTBdiff = pt[col * 2 + 1] - pt[row * wP1 * 2 + 1]
				local yRightTBdiff = pt[col * 2 + 2] - pt[row * wP1 * 2 + 2]
				xDiffSum = (xTopRLdiff + xRightTBdiff) * 0.5
				yDiffSum = (yTopRLdiff + yRightTBdiff) * 0.5
				xDiffDiff = (xTopRLdiff - xRightTBdiff) * 0.5
				yDiffDiff = (yTopRLdiff - yRightTBdiff) * 0.5
				xSum = xSum - 0.5 * (xDiffSum + xDiffDiff)
				ySum = ySum - 0.5 * (yDiffSum + yDiffDiff)
			end

			if (-2 < x and x < 3) and (-2 < y and y < 3) then
				if x <= 0 then
					if y <= 0 then
						local bj = 0.5 * (x + 2)
						local bi = 0.5 * (y + 2)
						if bj + bi <= 1 then
							dst[i] = interp(xSum - 2 * xDiffSum - 2 * xDiffDiff, xSum - 2 * xDiffDiff, xSum - 2 * xDiffSum, bj, bi)
							dst[i + 1] = interp(ySum - 2 * yDiffSum - 2 * yDiffDiff, ySum - 2 * yDiffDiff, ySum - 2 * yDiffSum, bj, bi)
						else
							dst[i] = interp(pt[1], xSum - 2 * xDiffSum, xSum - 2 * xDiffDiff, 1 - bj, 1 - bi)
							dst[i + 1] = interp(pt[2], ySum - 2 * yDiffSum, ySum - 2 * yDiffDiff, 1 - bj, 1 - bi)
						end
					elseif y >= 1 then
						local bj = 0.5 * (x + 2)
						local bi = 0.5 * (y - 1)
						if bj + bi <= 1 then
							dst[i] = interp(
								xSum - 2 * xDiffSum + 1 * xDiffDiff,
								pt[(row * wP1) * 2 + 1],
								xSum - 2 * xDiffSum + 3 * xDiffDiff,
								bj, bi
							)
							dst[i + 1] = interp(
								ySum - 2 * yDiffSum + 1 * yDiffDiff,
								pt[(row * wP1) * 2 + 2],
								ySum - 2 * yDiffSum + 3 * yDiffDiff,
								bj, bi
							)
						else
							dst[i] = interp(
								xSum + 3 * xDiffDiff,
								xSum - 2 * xDiffSum + 3 * xDiffDiff,
								pt[(row * wP1) * 2 + 1],
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								ySum + 3 * yDiffDiff,
								ySum - 2 * yDiffSum + 3 * yDiffDiff,
								pt[(row * wP1) * 2 + 2],
								1 - bj, 1 - bi
							)
						end
					else
						local aH = floor(a7)
						if aH == row then aH = row - 1 end
						local bj = 0.5 * (x + 2)
						local bi = a7 - aH
						local a9 = (aH + 1) / row
						if bj + bi <= 1 then
							local bb = aH / row
							dst[i] = interp(
								xSum - 2 * xDiffSum + bb * xDiffDiff,
								pt[(aH * wP1) * 2 + 1],
								xSum - 2 * xDiffSum + a9 * xDiffDiff,
								bj, bi
							)
							dst[i + 1] = interp(
								ySum - 2 * yDiffSum + bb * yDiffDiff,
								pt[(aH * wP1) * 2 + 2],
								ySum - 2 * yDiffSum + a9 * yDiffDiff,
								bj, bi
							)
						else
							dst[i] = interp(
								pt[((aH + 1) * wP1) * 2 + 1],
								xSum - 2 * xDiffSum + a9 * xDiffDiff,
								pt[(aH * wP1) * 2 + 1],
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								pt[((aH + 1) * wP1) * 2 + 2],
								ySum - 2 * yDiffSum + a9 * yDiffDiff,
								pt[(aH * wP1) * 2 + 2],
								1 - bj, 1 - bi
							)
						end
					end
				elseif 1 <= x then
					if y <= 0 then
						local bj = 0.5 * (x - 1)
						local bi = 0.5 * (y + 2)
						if bj + bi <= 1 then
							dst[i] = interp(
								xSum + 1 * xDiffSum - 2 * xDiffDiff,
								xSum + 3 * xDiffSum - 2 * xDiffDiff,
								pt[col * 2 + 1],
								bj, bi
							)
							dst[i + 1] = interp(
								ySum + 1 * yDiffSum - 2 * yDiffDiff,
								ySum + 3 * yDiffSum - 2 * yDiffDiff,
								pt[col * 2 + 2],
								bj, bi
							)
						else
							dst[i] = interp(
								xSum + 3 * xDiffSum,
								pt[col * 2 + 1],
								xSum + 3 * xDiffSum - 2 * xDiffDiff,
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								ySum + 3 * yDiffSum,
								pt[col * 2 + 2],
								ySum + 3 * yDiffSum - 2 * yDiffDiff,
								1 - bj, 1 - bi
							)
						end
					elseif y >= 1 then
						local bj = 0.5 * (x - 1)
						local bi = 0.5 * (y - 1)
						if bj + bi <= 1 then
							dst[i] = interp(
								pt[(col + row * wP1) * 2 + 1],
								xSum + 3 * xDiffSum + 1 * xDiffDiff,
								xSum + 1 * xDiffSum + 3 * xDiffDiff,
								bj, bi
							)
							dst[i + 1] = interp(
								pt[(col + row * wP1) * 2 + 2],
								ySum + 3 * yDiffSum + 1 * yDiffDiff,
								ySum + 1 * yDiffSum + 3 * yDiffDiff,
								bj, bi
							)
						else
							dst[i] = interp(
								xSum + 3 * xDiffSum + 3 * xDiffDiff,
								xSum + 1 * xDiffSum + 3 * xDiffDiff,
								xSum + 3 * xDiffSum + 1 * xDiffDiff,
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								ySum + 3 * yDiffSum + 3 * yDiffDiff,
								ySum + 1 * yDiffSum + 3 * yDiffDiff,
								ySum + 3 * yDiffSum + 1 * yDiffDiff,
								1 - bj, 1 - bi
							)
						end
					else
						local aH = floor(a7)
						if aH == row then aH = row - 1 end
						local bj = 0.5 * (x - 1)
						local bi = a7 - aH
						local bb = aH / row
						if bj + bi <= 1 then
							dst[i] = interp(
								pt[(col + aH * wP1) * 2 + 1],
								xSum + 3 * xDiffSum + bb * xDiffDiff,
								pt[(col + (aH + 1) * wP1) * 2 + 1],
								bj, bi
							)
							dst[i + 1] = interp(
								pt[(col + aH * wP1) * 2 + 2],
								ySum + 3 * yDiffSum + bb * yDiffDiff,
								pt[(col + (aH + 1) * wP1) * 2 + 2],
								bj, bi
							)
						else
							local a9 = (aH + 1) / row
							dst[i] = interp(
								xSum + 3 * xDiffSum + a9 * xDiffDiff,
								pt[(col + (aH + 1) * wP1) * 2 + 1],
								xSum + 3 * xDiffSum + bb * xDiffDiff,
								1 - bj, 1 - bi
							)
							dst[i] = interp(
								ySum + 3 * yDiffSum + a9 * yDiffDiff,
								pt[(col + (aH + 1) * wP1) * 2 + 2],
								ySum + 3 * yDiffSum + bb * yDiffDiff,
								1 - bj, 1 - bi
							)
						end
					end
				else
					if y <= 0 then
						local aY = floor(bd)
						if aY == col then aY = col - 1 end
						local bj = bd - aY
						local bi = 0.5 * (y + 2)
						local bo = (aY + 1) / col
						if bj + bi <= 1 then
							local bp = aY / col
							dst[i] = interp(
								xSum + bp * xDiffSum - 2 * xDiffDiff,
								xSum + bo * xDiffSum - 2 * xDiffDiff,
								pt[aY * 2 + 1],
								bj, bi
							)
							dst[i + 1] = interp(
								ySum + bp * yDiffSum - 2 * yDiffDiff,
								ySum + bo * yDiffSum - 2 * yDiffDiff,
								pt[aY * 2 + 2],
								bj, bi
							)
						else
							dst[i] = interp(
								pt[(aY + 1) * 2 + 1],
								pt[aY * 2 + 1],
								xSum + bo * xDiffSum - 2 * xDiffDiff,
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								pt[(aY + 1) * 2 + 2],
								pt[aY * 2 + 2],
								ySum + bo * yDiffSum - 2 * yDiffDiff,
								1 - bj, 1 - bi
							)
						end
					elseif y >= 1 then
						local aY = floor(bd)
						if aY == col then aY = col - 1 end
						local bj = bd - aY
						local bi = 0.5 * (y - 1)
						local bp = aY / col
						if bj + bi <= 1 then
							dst[i] = interp(
								pt[(aY + row * wP1) * 2 + 1],
								pt[((aY + 1) + row * wP1) * 2 + 1],
								xSum + bp * xDiffSum + 3 * xDiffDiff,
								bj, bi
							)
							dst[i + 1] = interp(
								pt[(aY + row * wP1) * 2 + 2],
								pt[((aY + 1) + row * wP1) * 2 + 2],
								ySum + bp * yDiffSum + 3 * yDiffDiff,
								bj, bi
							)
						else
							local bo = (aY + 1) / col
							dst[i] = interp(
								xSum + bo * xDiffSum + 3 * xDiffDiff,
								xSum + bp * xDiffSum + 3 * xDiffDiff,
								pt[((aY + 1) + row * wP1) * 2 + 1],
								1 - bj, 1 - bi
							)
							dst[i + 1] = interp(
								ySum + bo * yDiffSum + 3 * yDiffDiff,
								ySum + bp * yDiffSum + 3 * yDiffDiff,
								pt[((aY + 1) + row * wP1) * 2 + 2],
								1 - bj, 1 - bi
							)
						end
					else
						error(string.format("error calc: (%.4f, %.4f) @BDBoxGrid:transformPoints", x, y))
					end
				end
			else
				dst[i] = xSum + x * xDiffSum + y * xDiffDiff
				dst[i + 1] = ySum + x * yDiffSum + y * yDiffDiff
			end
		else
			local bn = bd - floor(bd)
			local bm = a7 - floor(a7)
			local aV = 2 * (floor(bd) + floor(a7) * (col + 1)) + 1
			if bn + bm < 1 then
				dst[i] = interp(pt[aV], pt[aV + 2], pt[aV + 2 * (col + 1)], bn, bm)
				dst[i + 1] = interp(pt[aV + 1], pt[aV + 3], pt[aV + 2 * (col + 1) + 1], bn, bm)
			else
				dst[i] = interp(pt[aV + 2 * (col + 1) + 2], pt[aV + 2 * (col + 1)], pt[aV + 2], 1 - bn, 1 - bm)
				dst[i + 1] = interp(pt[aV + 2 * (col + 1) + 3], pt[aV + 2 * (col + 1) + 1], pt[aV + 3], 1 - bn, 1 - bm)
			end
		end
	end
end

function BDBoxGrid:getNumPts()
	return (self.col + 1) * (self.row + 1)
end

function BDBoxGrid.getType()
	return BDBoxGrid
end

return BDBoxGrid
