local path = (...):sub(1, #(...) - #(".base.IBaseContext"))
local Kasumy = require(path..".dummy")

local IBaseContext = Kasumy.Luaoop.class("Kasumy.IBaseContext")

function IBaseContext:__construct(src)
	self.srcPtr = assert(src, "source base data is nil")
	self.partsIndex = nil
	self.outsideParam = false
	self.available = true
	self.srcPtr = src
	self.totalScale = 1
	self.interpolatedOpacity = 1
	self.totalOpacity = 1
end

function IBaseContext:isAvailable()
	return self.available and not(self.outsideParam)
end

function IBaseContext:setAvailable(avail)
	self.available = avail
end

function IBaseContext:getSrcPtr()
	return self.srcPtr
end

function IBaseContext:setPartsIndex(idx)
	self.partsIndex = idx
end

function IBaseContext:getPartsIndex()
	return self.partsIndex
end

function IBaseContext:isOutsideParam()
	return self.outsideParam
end

function IBaseContext:setOutsideParam(op)
	self.outsideParam = op
end

function IBaseContext:getTotalScale()
	return self.totalScale
end

function IBaseContext:setTotalScale_notForClient(scale)
	self.totalScale = scale
end

function IBaseContext:getInterpolatedOpacity()
	return self.interpolatedOpacity
end

function IBaseContext:setInterpolatedOpacity(opacity)
	self.interpolatedOpacity = opacity
end

function IBaseContext:getTotalOpacity()
	return self.totalOpacity
end

function IBaseContext:setTotalOpacity(opacity)
	self.totalOpacity = opacity
end

return IBaseContext
