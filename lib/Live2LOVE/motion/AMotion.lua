local path = (...):sub(1, #(...) - #(".motion.AMotion"))
local Kasumy = require(path..".dummy")

local UtSystem = require(path..".util.UtSystem")

local AMotion = Kasumy.Luaoop.class("Kasumy.AMotion")

local cos = math.cos
local min = math.min
local max = math.max
local pi = math.pi

local function getEasingSine(value)
	return 0.5 - 0.5 * cos(min(max(value, 0), 1) * pi)
end

function AMotion:__construct()
	self.fadeInMsec = 1
	self.fadeOutMsec = 1
	self.weight = 1
end

function AMotion:updateParam(model, motionQueueEnt)
	if not(motionQueueEnt.available) or motionQueueEnt.finished then
		return
	end

	local elapsedTime = UtSystem.getUserTimeMSec()
	if motionQueueEnt.startTimeMSec < 0 then
		motionQueueEnt.startTimeMSec = elapsedTime
		motionQueueEnt.fadeInStartTimeMSec = elapsedTime

		local dur = self:getDurationMSec()
		if motionQueueEnt.endTimeMSec <= 0 then
			motionQueueEnt.endTimeMSec = dur <= 0 and -1 or (motionQueueEnt.startTimeMSec + dur)
		end
	end

	local aH = self.fadeInMsec == 0 and 1 or getEasingSine(
		(elapsedTime - motionQueueEnt.fadeInStartTimeMSec) / self.fadeInMsec
	)
	local aK = (self.fadeOutMsec == 0 or motionQueueEnt.endTimeMSec < 0) and 1 or getEasingSine(
		(motionQueueEnt.endTimeMSec - elapsedTime) / self.fadeOutMsec
	)
	local weight = self.weight * aH * aK
	assert(0 <= weight and weight <= 1)

	self:updateParamExe(model, elapsedTime, weight, motionQueueEnt)
	motionQueueEnt.finished = motionQueueEnt.endTimeMSec > 0 and motionQueueEnt.endTimeMSec < elapsedTime
end

function AMotion:setFadeIn(fadeInMsec)
	self.fadeInMsec = fadeInMsec
end

function AMotion:setFadeOut(fadeOutMsec)
	self.fadeOutMsec = fadeOutMsec
end

function AMotion:getFadeIn()
	return self.fadeInMsec
end

function AMotion:getFadeOut()
	return self.fadeOutMsec
end

function AMotion:setWeight(weight)
	self.weight = weight
end

function AMotion:getWeight()
	return self.weight
end

function AMotion.getDurationMSec()
	return -1
end

function AMotion.getLoopDurationMSec()
	return -1
end

function AMotion.setOffsetMSec()
	error("pure virtual method 'AMotion:setOffsetMSec'")
end

function AMotion.reinit()
end

function AMotion.updateParamExe()
	error("pure virtual method 'AMotion:updateParamExe'")
end

return AMotion
