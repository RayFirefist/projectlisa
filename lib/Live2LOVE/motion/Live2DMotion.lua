local path = (...):sub(1, #(...) - #(".motion.Live2DMotion"))
local Kasumy = require(path..".dummy")

local AMotion = require(path..".motion.AMotion")
local UtSystem = require(path..".util.UtSystem")

local Motion = Kasumy.Luaoop.class("Kasumy.Motion")
local Live2DMotion = Kasumy.Luaoop.class("Kasumy.Live2DMotion", AMotion)

local floor = math.floor

local function lerp(v0, v1, t)
	return (1 - t) * v0 + t * v1
end

-- Motion
Motion.MOTION_TYPE_PARAM = 0
Motion.MOTION_TYPE_PARTS_VISIBLE = 1
Motion.MOTION_TYPE_LAYOUT_X = 100
Motion.MOTION_TYPE_LAYOUT_Y = 101
Motion.MOTION_TYPE_LAYOUT_ANCHOR_X = 102
Motion.MOTION_TYPE_LAYOUT_ANCHOR_Y = 103
Motion.MOTION_TYPE_LAYOUT_SCALE_X = 104
Motion.MOTION_TYPE_LAYOUT_SCALE_Y = 105

function Motion:__construct()
	self.paramIDStr = nil
	self.values = nil
	self.motionType = nil
end

-- Live2DMotion
function Live2DMotion:__construct()
	AMotion.__construct(self)
	self.motions = {}
	self.srcFps = 30
	self.maxLength = 0
	self.loop = false
	self.loopFadeIn = true
	self.loopDurationMSec = -1
	self.lastWeight = 0
end

function Live2DMotion.loadMotion(f)
	f = UtSystem.getFileContents(f, 'loadMotion', 1)
	f = f:gsub("\r\n", "\n"):gsub("\r", "\n")
	local ret = Live2DMotion()

	for line in f:gmatch("[^\n]+") do
		if line:find("#", 1, true) ~= 1 then
			local var, val = line:match("([^=]+)=?(.*)")

			if var == "$fps" then
				ret.srcFps = assert(tonumber(val), "invalid fps")
			elseif var:find("$", 1, true) ~= 1 then
				local motion = Motion()

				if var:find("VISIBLE:", 1, true) == 1 then
					motion.motionType = Motion.MOTION_TYPE_PARTS_VISIBLE
					motion.paramIDStr = var:sub(9)
				elseif var:find("LAYOUT:", 1, true) == 1 then
					local t = var:sub(8)
					if t == "ANCHOR_X" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_ANCHOR_X
					elseif t == "ANCHOR_Y" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_ANCHOR_Y
					elseif t == "SCALE_X" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_SCALE_X
					elseif t == "SCALE_Y" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_SCALE_Y
					elseif t == "X" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_X
					elseif t == "Y" then
						motion.motionType = Motion.MOTION_TYPE_LAYOUT_Y
					end

					motion.paramIDStr = t
				else
					motion.motionType = Motion.MOTION_TYPE_PARAM
					motion.paramIDStr = var
				end

				ret.motions[#ret.motions + 1] = motion
				local motionValuesLength

				if Kasumy.ffi then
					local mvals = {0}
					for w in val:gmatch("[^,|^ |^\t]+") do
						mvals[#mvals + 1] = tonumber(w)
					end
					motionValuesLength = #mvals - 1
					motion.values = Kasumy.ffi.new("double[?]", #mvals, mvals)
					motion.values[0] = motionValuesLength
				else
					local motionValues = {}
					for w in val:gmatch("[^,|^ |^\t]+") do
						motionValues[#motionValues + 1] = tonumber(w)
					end
					motionValues[0] = #motionValues
					motionValuesLength = #motionValues
					motion.values = motionValues
				end

				ret.maxLength = math.max(ret.maxLength, motionValuesLength)
			end
		end
	end

	ret.loopDurationMSec = (1000 * ret.maxLength) / ret.srcFps
	return ret
end

function Live2DMotion:updateParamExe(model, timeMsec, weight, motionQueueEnt)
	local elapsedTime = timeMsec - motionQueueEnt.startTimeMSec
	local a0 = elapsedTime * self.srcFps / 1000
	local aK = floor(a0)
	local aR = a0 - aK

	for i = 1, #self.motions do
		local motion = self.motions[i]
		local valuesLength = motion.values[0] or #motion.values
		local paramID = motion.paramIDStr

		if motion.motionType == Motion.MOTION_TYPE_PARTS_VISIBLE then
			model:setParamFloat(paramID, motion.values[(aK >= valuesLength and (valuesLength - 1) or aK) + 1])
		elseif not(
			Motion.MOTION_TYPE_LAYOUT_X <= motion.motionType and
			motion.motionType <= Motion.MOTION_TYPE_LAYOUT_SCALE_Y
		) then
			local paramIndex = model:getParamIndex(paramID)
			local mdc = model:getModelContext()
			local paramMax = mdc:getParamMax(paramIndex)
			local paramMin = mdc:getParamMin(paramIndex)
			local aS = 0.4 * (paramMax - paramMin)
			local paramValue = mdc:getParamFloat(paramIndex)
			local a2 = motion.values[(aK >= valuesLength and (valuesLength - 1) or aK) + 1]
			local a1 = motion.values[(aK + 1 >= valuesLength and (valuesLength - 1) or (aK + 1)) + 1]
			local aI
			if (a2 < a1 and a1 - a2 > aS) or (a2 > a1 and a2 - a1 > aS) then aI = a2
			else aI = lerp(a2, a1, aR) end

			model:setParamFloat(paramID, lerp(paramValue, aI, weight))
		end
	end

	if aK >= self.maxLength then
		if self.loop then
			motionQueueEnt.startTimeMSec = timeMsec

			if self.loopFadeIn then
				motionQueueEnt.fadeInStartTimeMSec = timeMsec
			end
		else
			motionQueueEnt.finished = true
		end
	end

	self.lastWeight = weight
end

function Live2DMotion:setLoop(loop)
	self.loop = loop
end

function Live2DMotion:isLoop()
	return self.loop
end

function Live2DMotion:setLoopFadeIn(loopFadeIn)
	self.loopFadeIn = loopFadeIn
end

function Live2DMotion:isLoopFadeIn()
	return self.loopFadeIn > 0
end

function Live2DMotion:getDurationMSec()
	return self.loop and -1 or self.loopDurationMSec
end

function Live2DMotion:getLoopDurationMSec()
	return self.loopDurationMSec
end

return Live2DMotion
