local path = (...):sub(1, #(...) - #(".motion.MotionQueueManager"))
local Kasumy = require(path..".dummy")

local MotionQueueEnt = require(path..".motion.MotionQueueEnt")

local MotionQueueManager = Kasumy.Luaoop.class("Kasumy.MotionQueueManager")

function MotionQueueManager:__construct()
	self.motionDebugMode = false
	self.motions = {}
end

function MotionQueueManager:startMotion(motion)
	for i = 1, #self.motions do
		local m = self.motions[i]
		if m then
			m:startFadeout(m.motion:getFadeOut())
		end
	end

	if not(motion) then
		return -1
	end

	local m = MotionQueueEnt()
	m.motion = motion
	self.motions[#self.motions + 1] = m

	return m.motionQueueEntNo
end

function MotionQueueManager:updateParam(model)
	local aI = false
	local len = #self.motions
	local index = 1
	local left = len

	for i = 1, len do
		local motionQueueEnt = self.motions[i]
		if motionQueueEnt.motion then
			motionQueueEnt.motion:updateParam(model, motionQueueEnt)
			aI = true

			if motionQueueEnt:isFinished() then
				left = left - 1
			else
				self.motions[index] = self.motions[i]
				index = index + 1
			end
		else
			left = left - 1
		end
	end

	for i = left + 1, len do
		self.motions[i] = nil
	end

	return aI
end

function MotionQueueManager:isFinished(motionQueueEntNo)
	if motionQueueEntNo then
		for i = #self.motions, 1, -1 do
			local motionQueueEnt = self.motions[i]

			if motionQueueEnt.motionQueueEntNo == motionQueueEntNo and not(motionQueueEnt:isFinished()) then
				return false
			end
		end
	else
		for i = #self.motions, 1, -1 do
			local motionQueueEnt = self.motions[i]

			if motionQueueEnt.motion then
				if not(motionQueueEnt:isFinished()) then
					return false
				end
			else
				table.remove(self.motions, i)
			end
		end
	end

	return true
end

function MotionQueueManager:stopAllMotions()
	for i = #self.motions, 1, -1 do
		self.motions[i] = nil
	end
end

return MotionQueueManager
