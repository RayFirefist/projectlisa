local path = (...):sub(1, #(...) - #(".motion.MotionQueueEnt"))
local Kasumy = require(path..".dummy")

local UtSystem = require(path..".util.UtSystem")

local MotionQueueEnt = Kasumy.Luaoop.class("Kasumy.MotionQueueEnt")
local static_motionQueueEntNo = 0

function MotionQueueEnt:__construct()
	self.motion = nil
	self.available = true
	self.finished = false
	self.startTimeMSec = -1
	self.fadeInStartTimeMSec = -1
	self.endTimeMSec = -1
	static_motionQueueEntNo = static_motionQueueEntNo + 1
	self.motionQueueEntNo = static_motionQueueEntNo
end

function MotionQueueEnt:isFinished()
	return self.finished
end

function MotionQueueEnt:startFadeout(fadeOutMsec)
	local time = UtSystem.getUserTimeMSec()
	local t = time + fadeOutMsec
	if self.endTimeMSec < 0 or t < self.endTimeMSec then
		self.endTimeMSec = t
	end
end

function MotionQueueEnt:getMotionQueueEntNo()
	return self.motionQueueEntNo
end

return MotionQueueEnt
