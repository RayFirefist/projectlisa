local UtInterpolate = {}
local floor = math.floor

local function lerp(v0, v1, t)
	return (1 - t) * v0 + t * v1
end

local function lerp2(v0, v1, t)
	return v0 + (v1 - v0) * t
end

function UtInterpolate.interpolateInt(mdc, pivotManager, pivotValue)
	local a1, paramOutside = pivotManager:calcPivotValue(mdc)
	local a3 = mdc:getTmpPivotTableIndicesRef()
	local ba = mdc:getTmpT_ArrayRef()
	pivotManager:calcPivotIndexies(a3, ba, a1)

	if a1 <= 0 then
		return pivotValue[a3[1] + 1], paramOutside
	elseif a1 == 1 then
		return lerp2(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], ba[1]), paramOutside
	elseif a1 == 2 then
		local l1 = ba[1]
		return lerp2(
			floor(lerp2(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1)),
			floor(lerp2(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1)),
			ba[2]
		), paramOutside
	elseif a1 == 3 then
		local l1, l2 = ba[1], ba[2]
		return lerp2(
			floor(lerp2(
				floor(lerp2(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1)),
				floor(lerp2(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1)),
				l2
			)),
			floor(lerp2(
				floor(lerp2(pivotValue[a3[5] + 1], pivotValue[a3[6] + 1], l1)),
				floor(lerp2(pivotValue[a3[7] + 1], pivotValue[a3[8] + 1], l1)),
				l2
			)),
			ba[3]
		), paramOutside
	elseif a1 == 4 then
		local l1, l2, l3 = ba[1], ba[2], ba[3]
		return lerp2(
			floor(lerp2(
				floor(lerp2(
					floor(lerp2(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1)),
					floor(lerp2(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1)),
					l2
				)),
				floor(lerp2(
					floor(lerp2(pivotValue[a3[5] + 1], pivotValue[a3[6] + 1], l1)),
					floor(lerp2(pivotValue[a3[7] + 1], pivotValue[a3[8] + 1], l1)),
					l2
				)),
				l3
			)),
			floor(lerp2(
				floor(lerp2(
					floor(lerp2(pivotValue[a3[9] + 1], pivotValue[a3[10] + 1], l1)),
					floor(lerp2(pivotValue[a3[11] + 1], pivotValue[a3[12] + 1], l1)),
					l2
				)),
				floor(lerp2(
					floor(lerp2(pivotValue[a3[13] + 1], pivotValue[a3[14] + 1], l1)),
					floor(lerp2(pivotValue[a3[15] + 1], pivotValue[a3[16] + 1], l1)),
					l2
				)),
				l3
			)),
			ba[4]
		), paramOutside
	else
		local aV = 2 ^ a1
		local aY = {}

		for i = 1, aV do
			local aI = i - 1
			local aH = 1
			for j = 1, a1 do
				aH = aH * (aI % 2 == 0 and (1 - ba[j]) or ba[j])
				aI = aI * 0.5
			end

			aY[i] = aH
		end

		local bs = {}
		for i = 1, aV do
			bs[i] = pivotValue[a3[i] + 1]
		end

		local bd = 0
		for i = 1, aV do
			bd = bd + aY[i] * bs[i]
		end

		return floor(bd + 0.5), paramOutside
	end
end

function UtInterpolate.interpolateFloat(mdc, pivotManager, pivotValue)
	local a1, paramOutside = pivotManager:calcPivotValue(mdc)
	local a3 = mdc:getTmpPivotTableIndicesRef()
	local ba = mdc:getTmpT_ArrayRef()
	pivotManager:calcPivotIndexies(a3, ba, a1)

	if a1 <= 0 then
		return pivotValue[a3[1] + 1], paramOutside
	elseif a1 == 1 then
		return lerp(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], ba[1]), paramOutside
	elseif a1 == 2 then
		local l1 = ba[1]
		return lerp(
			lerp(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1),
			lerp(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1),
			ba[2]
		), paramOutside
	elseif a1 == 3 then
		local l1, l2 = ba[1], ba[2]
		return lerp(
			lerp(
				lerp(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1),
				lerp(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1),
				l2
			),
			lerp(
				lerp(pivotValue[a3[5] + 1], pivotValue[a3[6] + 1], l1),
				lerp(pivotValue[a3[7] + 1], pivotValue[a3[8] + 1], l1),
				l2
			),
			ba[3]
		), paramOutside
	elseif a1 == 4 then
		local l1, l2, l3 = ba[1], ba[2], ba[3]
		return lerp(
			lerp(
				lerp(
					lerp(pivotValue[a3[1] + 1], pivotValue[a3[2] + 1], l1),
					lerp(pivotValue[a3[3] + 1], pivotValue[a3[4] + 1], l1),
					l2
				),
				lerp(
					lerp(pivotValue[a3[5] + 1], pivotValue[a3[6] + 1], l1),
					lerp(pivotValue[a3[7] + 1], pivotValue[a3[8] + 1], l1),
					l2
				),
				l3
			),
			lerp(
				lerp(
					lerp(pivotValue[a3[9] + 1], pivotValue[a3[10] + 1], l1),
					lerp(pivotValue[a3[11] + 1], pivotValue[a3[12] + 1], l1),
					l2
				),
				lerp(
					lerp(pivotValue[a3[13] + 1], pivotValue[a3[14] + 1], l1),
					lerp(pivotValue[a3[15] + 1], pivotValue[a3[16] + 1], l1),
					l2
				),
				l3
			),
			ba[4]
		), paramOutside
	else
		local aV = 2 ^ a1
		local aY = {}

		for i = 1, aV do
			local aI = i - 1
			local aH = 1
			for j = 1, a1 do
				aH = aH * (aI % 2 == 0 and (1 - ba[j]) or ba[j])
				aI = aI * 0.5
			end

			aY[i] = aH
		end

		local bs = {}
		for i = 1, aV do
			bs[i] = pivotValue[a3[i] + 1]
		end

		local bd = 0
		for i = 1, aV do
			bd = bd + aY[i] * bs[i]
		end

		return bd, paramOutside
	end
end

function UtInterpolate.interpolatePoints(mdc, pivotManager, numPts, pivotPoints, dst_points, pt_offset, pt_step)
	local aN, paramOutside = pivotManager:calcPivotValue(mdc)
	local bw = mdc:getTmpPivotTableIndicesRef()
	local a2 = mdc:getTmpT_ArrayRef()
	local aJ = numPts * 2
	local aQ = pt_offset + 1
	pivotManager:calcPivotIndexies(bw, a2, aN)

	if aN <= 0 then
		local bq = pivotPoints[bw[1] + 1]
		for j = 1, aJ, 2 do
			dst_points[aQ] = bq[j]
			dst_points[aQ + 1] = bq[j + 1]
			aQ = aQ + pt_step
		end

		return paramOutside
	elseif aN == 1 then
		local bq = pivotPoints[bw[1] + 1]
		local bp = pivotPoints[bw[2] + 1]
		for j = 1, aJ, 2 do
			dst_points[aQ] = lerp(bq[j], bp[j], a2[1])
			dst_points[aQ + 1] = lerp(bq[j + 1], bp[j + 1], a2[1])
			aQ = aQ + pt_step
		end

		return paramOutside
	elseif aN == 2 then
		-- I'm pretty sure this is bilinear interpolation
		local bq = pivotPoints[bw[1] + 1]
		local bp = pivotPoints[bw[2] + 1]
		local aZ = pivotPoints[bw[3] + 1]
		local aY = pivotPoints[bw[4] + 1]
		local bT = 1 - a2[1]
		local bP = 1 - a2[2]
		local b2 = bP * bT
		local b0 = bP * a2[1]
		local bM = a2[2] * bT
		local bL = a2[2] * a2[1]
		for j = 1, aJ, 2 do
			dst_points[aQ] = b2 * bq[j] + b0 * bp[j] + bM * aZ[j] + bL * aY[j]
			dst_points[aQ + 1] = b2 * bq[j + 1] + b0 * bp[j + 1] + bM * aZ[j + 1] + bL * aY[j + 1]
			aQ = aQ + pt_step
		end

		return paramOutside
	elseif aN == 3 then
		-- Welp, trilinear interpolation
		local ba = pivotPoints[bw[1] + 1]
		local a9 = pivotPoints[bw[2] + 1]
		local aP = pivotPoints[bw[3] + 1]
		local aO = pivotPoints[bw[4] + 1]
		local a6 = pivotPoints[bw[5] + 1]
		local a4 = pivotPoints[bw[6] + 1]
		local aL = pivotPoints[bw[7] + 1]
		local aK = pivotPoints[bw[8] + 1]
		local bT = 1 - a2[1]
		local bP = 1 - a2[2]
		local bN = 1 - a2[3]
		local b8 = bN * bP * bT
		local b7 = bN * bP * a2[1]
		local bU = bN * a2[2] * bT
		local bS = bN * a2[2] * a2[1]
		local b6 = a2[3] * bP * bT
		local b5 = a2[3] * bP * a2[1]
		local bQ = a2[3] * a2[2] * bT
		local bO = a2[3] * a2[2] * a2[1]
		for j = 1, aJ, 2 do
			-- luacheck: push no max line length
			dst_points[aQ] = b8 * ba[j] + b7 * a9[j] + bU * aP[j] + bS * aO[j] + b6 * a6[j] + b5 * a4[j] + bQ * aL[j] + bO * aK[j]
			j = j + 1
			dst_points[aQ + 1] = b8 * ba[j] + b7 * a9[j] + bU * aP[j] + bS * aO[j] + b6 * a6[j] + b5 * a4[j] + bQ * aL[j] + bO * aK[j]
			aQ = aQ + pt_step
			-- luacheck: pop
		end

		return paramOutside
	elseif aN == 4 then
		-- Excuse me what the fuck?
		local bD = pivotPoints[bw[1] + 1]
		local bB = pivotPoints[bw[2] + 1]
		local bo = pivotPoints[bw[3] + 1]
		local bm = pivotPoints[bw[4] + 1]
		local by = pivotPoints[bw[5] + 1]
		local bx = pivotPoints[bw[6] + 1]
		local be = pivotPoints[bw[7] + 1]
		local bd = pivotPoints[bw[8] + 1]
		local bG = pivotPoints[bw[9] + 1]
		local bE = pivotPoints[bw[10] + 1]
		local bv = pivotPoints[bw[11] + 1]
		local bu = pivotPoints[bw[12] + 1]
		local bA = pivotPoints[bw[13] + 1]
		local bz = pivotPoints[bw[14] + 1]
		local bn = pivotPoints[bw[15] + 1]
		local bl = pivotPoints[bw[16] + 1]
		local bT = 1 - a2[1]
		local bP = 1 - a2[2]
		local bN = 1 - a2[3]
		local bK = 1 - a2[4]
		local bk = bK * bN * bP * bT
		local bi = bK * bN * bP * a2[1]
		local aW = bK * bN * a2[2] * bT
		local aV = bK * bN * a2[2] * a2[1]
		local bc = bK * a2[3] * bP * bT
		local bb = bK * a2[3] * bP * a2[1]
		local aS = bK * a2[3] * a2[2] * bT
		local aR = bK * a2[3] * a2[2] * a2[1]
		local bs = a2[4] * bN * bP * bT
		local br = a2[4] * bN * bP * a2[1]
		local a1 = a2[4] * bN * a2[2] * bT
		local a0 = a2[4] * bN * a2[2] * a2[1]
		local bh = a2[4] * a2[3] * bP * bT
		local bf = a2[4] * a2[3] * bP * a2[1]
		local aU = a2[4] * a2[3] * a2[2] * bT
		local aT = a2[4] * a2[3] * a2[2] * a2[1]
		for j = 1, aJ, 2 do
			-- Sorry, had to do this
			-- luacheck: push no max line length
			dst_points[aQ] = bk * bD[j] + bi * bB[j] + aW * bo[j] + aV * bm[j] + bc * by[j] + bb * bx[j] + aS * be[j] + aR * bd[j] + bs * bG[j] + br * bE[j] + a1 * bv[j] + a0 * bu[j] + bh * bA[j] + bf * bz[j] + aU * bn[j] + aT * bl[j]
			j = j + 1
			dst_points[aQ + 1] = bk * bD[j] + bi * bB[j] + aW * bo[j] + aV * bm[j] + bc * by[j] + bb * bx[j] + aS * be[j] + aR * bd[j] + bs * bG[j] + br * bE[j] + a1 * bv[j] + a0 * bu[j] + bh * bA[j] + bf * bz[j] + aU * bn[j] + aT * bl[j]
			aQ = aQ + pt_step
			-- luacheck: pop
		end

		return paramOutside
	else
		-- Finally a generalized algorithm
		local b4 = 2 ^ aN
		local bJ = {}
		for i = 0, b4 - 1 do
			local aH = i
			local aM = 1
			for k = 1, aN do
				aM = aM * (aH % 2 == 0 and (1 - a2[k]) or a2[k])
				aH = aH / 2
			end
			bJ[i + 1] = aM
		end

		local bg = {}
		for n = 1, b4 do
			bg[n] = pivotPoints[bw[n] + 1]
		end

		for j = 1, aJ do
			local a8, a7 = 0, 0
			for n = 1, b4 do
				a8 = a8 + bJ[n] * bg[n][j]
				a7 = a7 + bJ[n] * bg[n][j + 1]
			end

			dst_points[aQ] = a8
			dst_points[aQ + 1] = a7
			aQ = aQ + pt_step
		end

		return paramOutside
	end
end

return UtInterpolate
