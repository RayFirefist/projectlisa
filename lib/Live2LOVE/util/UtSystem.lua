local love = require("love")

local UtSystem = {}
local userTimeMSec = -1

function UtSystem.getTimeMSec()
	return (love.timer and love.timer.getTime() or os.time()) * 1000
end

function UtSystem.getUserTimeMSec()
	return userTimeMSec == -1 and UtSystem.getTimeMSec() or userTimeMSec
end

function UtSystem.setUserTimeMSec(time)
	userTimeMSec = time
end

function UtSystem.updateUserTimeMSec()
	userTimeMSec = UtSystem.getTimeMSec()
end

function UtSystem.resetUserTimeMSec()
	userTimeMSec = -1
end

-- This function is used to convert various kinds
-- of data to raw string
function UtSystem.getFileContents(buf, f, n)
	local t = type(buf)
	if t == "string" then
		if #buf >= 512 or buf:find("\26", 1, true) or buf:find("\n", 1, true) then
			return buf
		else
			return love.filesystem.read(buf)
		end
	elseif t == "userdata" then
		if buf.typeOf then
			if buf:typeOf("Data") then
				return buf:getString()
			elseif buf:typeOf("File") then
				if buf:isOpen() then
					if buf:getMode() == "r" then
						return buf:read()
					else
						error("bad argument #"..n.." to '"..f.."' (file is not open for reading)", 3)
					end
				else
					assert(buf:open("r"))
					local ret = buf:read()
					buf:close()
					return ret
				end
			else
				error("bad argument #"..n.." to '"..f.."' (Data or File expected)", 3)
			end
		elseif tostring(buf):find("file (", 1, true) == 1 then
			return buf:read("*a")
		else
			error("bad argument #"..n.." to '"..f.."' (Data or File expected)", 3)
		end
	else
		error("bad argument #"..n.." to '"..f.."' (Data, File, or string expected)", 3)
	end
end

-- should not be used!
UtSystem.exit = os.exit

return UtSystem
