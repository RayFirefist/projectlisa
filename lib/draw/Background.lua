require("lib/class")
require("lib/draw/Texture")

--------------------------------------------------------------
--- [[BG]]
--------------------------------------------------------------

LiveBG = class()

function LiveBG:init(skin)

	r = love.graphics.getWidth()
	c = love.graphics.getHeight()

	self.image = love.graphics.newImage(skin)
	self.image_fever = nil
	self.offset = {-205,-70}
	-- self.scale = {4 * (r) / 3, nil}
	-- self.scale[2] = .75 * self.scale[1]
	
	x_bgskin = 3 * (r) / 3
	y_bgskin = .52 * (4 * (r) / 3)

	-- scalex, scaley = getImageScaleForNewDimensions(self.image, self.scale[1], self.scale[2])

	local scalex, scaley = getImageScaleForNewDimensions(self.image, x_bgskin, y_bgskin)
	self.scale = {scalex, scaley}
	--self.scale = {1, 1}

	-- print(scalex, scaley)

	local offsetX, offsetY = getImageOffsetScenarioBackground(self.image)

	self.offset = {offsetX, offsetY}
end

function LiveBG:setImages(skin)
	self.image = love.graphics.newImage(skin)
end

function LiveBG:isNilFever()
	return self.image_fever == nil
end

function LiveBG:getImage()
	return self.image
end

function LiveBG:getScale()
	return self.scale
end

function LiveBG:getOffset()
	return self.offset
end

function LiveBG:getX()
	return self.image:getWidth()
end

function LiveBG:getY()
	return self.image:getHeight()
end

function LiveBG:drawImage()
	love.graphics.draw(self.image, self.offset[1], self.offset[2], 0, self.scale[1], self.scale[2])
	-- print(self.image, self.offset[1], self.offset[2], 0, self.scale[1], self.scale[2])
end

function LiveBG:drawImageCustom(x, y, rotation, scaleX, scaleY, width, height)
	love.graphics.draw(self.image, x, y, rotation, scaleX, scaleY, width, height)
	-- print(self.image, self.offset[1], self.offset[2], 0, self.scale[1], self.scale[2])
end

function LiveBG:drawImageFever()
	love.graphics.draw(self.image_fever, self.offset[1], self.offset[2], 0, self.scale[1], self.scale[2])
end
