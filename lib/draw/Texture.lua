local width = love.graphics.getWidth()
local height = love.graphics.getHeight()

function getImageRatio(image)
	local currentWidth, currentHeight = image:getDimensions()
	return currentWidth / currentHeight
end

function getImageX(image)
	local width, height = image:getDimensions()
	return width
end

function getImageY(image)
	local width, height = image:getDimensions()
	return height
end

function getImageScaleForNewDimensions( image, newWidth, newHeight )
    local currentWidth, currentHeight = image:getDimensions()
    print(image:getDimensions())
    return ( newWidth / currentWidth ), ( newHeight / currentHeight )
end

function getImageOffsetScenarioBackground( image )
	local currentWidth, currentHeight = image:getDimensions()
	local newWidth, newHeight
	if currentHeight == currentWidth and currentWidth == 1024 then
		if (width/4) * 3 == height then
			print("4:3 detected")
			return 0, 0, currentWidth, currentHeight
		end
		if (width/16) * 9 == height then
			print("16:9 detected")
			return 0, -currentHeight/8, currentWidth, currentHeight
		end

		newHeight = (currentHeight- height)/2
		newHeight = 0
		return 0, newHeight, currentHeight, currentWidth
	else
		print((width-currentWidth) / 2, 0, currentWidth, currentHeight)
		return (width-currentWidth) / 2, 0, currentWidth, currentHeight
	end
end

function getLaneWidth()
	return width * .8
end

function getLaneHeight()
	return getLaneWidth() / 2 / .875
end

function getLaneScale()
	return getLaneScale(width) / 1080
end

function getLineOffset()
	return 1.225 * getLaneHeight() * (.5 - .225/1.225)
end

function centerX(inputX)
	print((width/2) - (inputX/2))
	return (width/2) - (inputX/2)
end

function centerY(inputY)
	print((height/2) - (inputY/2))
	return (height/2) - (inputY/2)
end

Texture = class()

function Texture:init(image)
	self.image = image
	self.offset = {nil,nil}
	self.ratio = {nil,nil}
end

function Texture:setOffset(x, y)
	self.offset[1] = x or 0.75
	self.offset[2] = y or 0.75
end

function Texture:setRatio(x, y)
	self.ratio[1] = x or 0.75
	self.ratio[2] = y or 0.75
end

function Texture:draw()
	love.graphics.draw(self.image, self.offset[1], self.offset[2], 0, self.scale[1], self.scale[2])
end
