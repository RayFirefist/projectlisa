love = require("love")
mkdir = require("lib/download/mkdir")
--require("lib/download/lfs")
require("socket.http")

-- https://bandori.ga/assets-jp/title/persona_2018_rip/title_bg.png

function explode(d,p)
   local t, ll
   t={}
   ll=0
   if(#p == 1) then
      return {p}
   end
   while true do
      l = string.find(p, d, ll, true) -- find the next d in the string
      if l ~= nil then -- if "not not" found then..
         table.insert(t, string.sub(p,ll,l-1)) -- Save it in our array.
         ll = l + 1 -- save just after where we found it for searching next time.
      else
         table.insert(t, string.sub(p,ll)) -- Save what's left in our array.
         break -- Break at end, as it should be, according to the lua manual.
      end
   end
   return t
end

function strjoin(delimiter, list)
   local len = getn(list)
   if len == 0 then
      return "" 
   end
   local string = list[1]
   for i = 2, len do 
      string = string .. delimiter .. list[i] 
   end
   return string
end

function getn (t)
  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count
end

function checkFile( filename, isModel )
	-- body

	local modelsHost = "https://bandori.makoo.eu/live2d/"
	local mediaHost = "https://bestdori.com:443/"

	isModel = isModel or false

	outFilename = love.filesystem.getSaveDirectory() .. "/" .. filename

	local exists = false

	-- File exists?
	-- Source https://stackoverflow.com/questions/4990990/check-if-a-file-exists-with-lua
	local f=io.open(outFilename,"r")
   	if f~=nil then 
   		io.close(f) 
   		exists=true
   	end

	if not exists then

		local host

		if isModel then
			print("MODEL")
			host = modelHost
		else
			print("MEDIA")
			host = mediaHost
		end

		-- retrieve the content of a URL
		print(host .. filename)
		require("socket")
		local http = require("socket.http")
		local body, code = http.request(host .. filename)
		print("BODY: (" .. body .. ")")
		if code~=200 then 
			assert(code)
			print(code)
			return filename
		end

		-- create folder
		local tempTableFile = explode("/", filename)
		print(tempTableFile)
		tempTableFile[getn(tempTableFile)] = ""
		love.filesystem.createDirectory(strjoin("/", tempTableFile))

		-- save the content to a file
		local success, message = love.filesystem.write(filename, body)
		if success == false then
			assert(message)
		end
		print(success)
		print(message)
	end

	return filename

end

return checkFile